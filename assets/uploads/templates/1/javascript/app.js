$('.list-item-collapse').click(function (event) {
    event.preventDefault();
    var $this = $(this);
    var $target = $this.next('.list-item-collapsed');
    if ($this.hasClass('expanded'))
    {
        $target.slideUp();
        $this.removeClass('expanded');
    }
    else
    {
        $target.slideDown();
        $this.addClass('expanded');
        $this.siblings('.list-item-collapse.expanded').removeClass('expanded').each(function (index, element) {
            $(element).next('.list-item-collapsed').slideUp();
        });
    }
})

$('.photo-galleries').each(function () {
    var $gallery = $(this);
    var $rows = $gallery.find('.photos .row');
    var $photos = $gallery.find('.photos a');
    if ($photos.length)
    {
        if ($rows.length == 1)
        {
            $gallery.find('.control').html('');
        }
        else
        {
            $gallery.find('.control a').click(function (e) {
                e.preventDefault();
                var $this = $(this);
                var direction = $this.attr('href').replace('#', '');
                var $row = $rows.filter(':visible').hide();
                var $next = $row[direction]();
                if ($next.length == 0 || $next.find('a').length == 0)
                {
                    $next = $row;
                }
                $next.show();
            });
        }
        $rows.slice(1).hide();
        $photos.click(function (e) {
            e.preventDefault();
            $gallery.find('.photo').hide().attr('src', $(this).attr('href')).fadeIn();
        });
        $photos.first().click();
    }
    else
    {
        $gallery.remove();
    }
});