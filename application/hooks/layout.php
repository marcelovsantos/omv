<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout
{

    public function __construct()
    {
        $this->CI = &get_instance();
        $method   = isset($this->method) ? $this->CI->method : $this->CI->router->method;
        $run      = FALSE;
        switch ($method)
        {
            case 'create':
                $method = 'neew';
                $run    = TRUE;
                break;
            case 'update':
                $method = 'edit';
                $run    = TRUE;
                break;
        }

        if ($run && ( $post = flatten_array($this->CI->input->post())))
        {
            $this->CI->load->library('form_validation');
            foreach ($post as $key => $value)
            {
                $this->CI->form_validation->set_rules($key, $key, 'required');
            }
            $run = $this->CI->form_validation->run();
        }

        if (!isset($this->CI->view))
        {
            $this->CI->view = "{$this->CI->router->directory}{$this->CI->router->class}/{$method}";
        }
    }

    public function init()
    {
        if (isset($this->CI->view_content))
        {
            $view = $this->CI->view_content;
        }
        elseif ($view = $this->CI->view)
        {
            $view = $this->CI->load->view("{$view}.php", $this->CI->data, TRUE);
        }

        if (isset($this->CI->template_content))
        {
            $template = $this->CI->template_content;
        }
        elseif ($template = $this->CI->template)
        {
            $this->CI->data['restrict'] = $this->CI->restrict;
            $template                   = $this->CI->load->view("{$template}.php", $this->CI->data, TRUE);
        }

        $errors = isset($this->CI->data['errors']) ? $this->CI->data['errors'] : array();
        $errors = implode('<br>', !is_array($errors) ? array($errors) : $errors);
        $errors = $errors ? '<div class="alert alert-warning" data-dismiss="alert">' . $errors . '</div>' : $errors;

        $data = array(
            'template_url'     => base_url($this->CI->template_path) . '/',
            'page_title'       => $this->CI->title,
            'page_keywords'    => $this->CI->keywords,
            'page_description' => $this->CI->description,
            'errors'           => $errors,
            'page_styles'      => $this->CI->styles,
            'page_scripts'     => $this->CI->scripts,
            'page_id'          => isset($this->CI->page_id) ? $this->CI->page_id : NULL,
            'page_content'     => $view,
        );

        $template = $this->CI->parser->parse_string($template, $data, TRUE);

        if ($this->CI->componentize_view)
        {
            $template = $this->CI->parser->componentize($template, array_merge($data, $this->CI->data));
        }

        $this->CI->output->set_output($template);
    }

}