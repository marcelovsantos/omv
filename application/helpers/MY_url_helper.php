<?php

if (!function_exists('base_url'))
{

    function base_url($uri = '')
    {
        $CI = & get_instance();
        if (strpos($uri,'{{old_url}}') !== FALSE && isset($CI->config->config['old_url']))
        {
            return str_replace('{{old_url}}', $CI->config->config['old_url'], $uri);
        }
        return $CI->config->base_url($uri);
    }

}

if (!function_exists('site_url'))
{

    function site_url($uri = '')
    {
        $CI = & get_instance();
        if (strpos($uri, '{{old_url}}') !== FALSE && isset($CI->config->config['old_url']))
        {
            return str_replace('{{old_url}}', $CI->config->config['old_url'], $uri);
        }
        return $CI->config->site_url($uri);
    }

}