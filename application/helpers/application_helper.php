<?php

function pagination($total, $config = array())
{
    $CI     = &get_instance();
    $CI->load->library('pagination');
    $default = array(
        'base_url'             => current_url() . '?',
        'page_query_string'    => true,
        'total_rows'           => $total,
        'per_page'             => $CI->per_page,
        'query_string_segment' => 'page',
        'num_links'            => 5,
        'full_tag_open'        => '<div class="pull-left"><ul class="pagination m-b-n m-t-sm">',
        'full_tag_close'       => '</ul></div>',
        'first_link'           => '&laquo;',
        'first_tag_open'       => '<li class="prev page">',
        'first_tag_close'      => '</li>',
        'last_link'            => '&raquo;',
        'last_tag_open'        => '<li class="next page">',
        'last_tag_close'       => '</li>',
        'next_link'            => NULL,
        'next_tag_open'        => NULL,
        'next_tag_close'       => NULL,
        'prev_link'            => NULL,
        'prev_tag_open'        => NULL,
        'prev_tag_close'       => NULL,
        'cur_tag_open'         => '<li class="active"><a>',
        'cur_tag_close'        => '</a></li>',
        'num_tag_open'         => '<li class="page">',
        'num_tag_close'        => '</li>',
    );
    $config = array_merge($default,$config);
    $CI->pagination->initialize($config);
    return $CI->pagination->create_links();
}

function get_flash()
{
    $CI    = & get_instance();
    if ($flash = $CI->input->get('flash'))
    {
        ob_start();
        foreach ($flash as $class => $message)
        {
            ?>
            <div role="alert" class="alert alert-<?= $class ?>">
                <button type="button" class="close" data-dismiss="alert">&times;</button>
                <?= $message ?>
            </div>
            <?php
        }
        return ob_get_clean();
    }
}
