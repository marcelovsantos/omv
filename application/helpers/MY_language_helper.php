<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

function t($line)
{
    $tline = lang($line);
    return $tline? $tline: $line;
}
