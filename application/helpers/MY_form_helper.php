<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

function objects_to_options($objects, $label, $active = array(), $allow_null = FALSE, $value = NULL)
{
    if (!$value)
    {
        $value = '{{id}}';
    }
    if (!is_array($active))
    {
        $active = array($active);
    }
    $CI   = & get_instance();
    $html = $allow_null ? '<option></option>' : '';

    foreach ($objects as $option)
    {
        $option_label = $CI->parser->parse_string($label, $option, TRUE);
        $option_value = $CI->parser->parse_string($value, $option, TRUE);
        $selected     = in_array($option_value, $active) ? 'selected' : NULL;
        $html.='<option value="' . $option_value . '" ' . $selected . '>' . $option_label . '</option>';
    }
    return $html;
}

function flatten_array($array, $prefix = '', $level = 0)
{
    $result = array();
    if (is_array($array))
    {
        foreach ($array as $key => $value)
        {
            if (is_array($value))
            {
                if ($prefix)
                {
                    $key = $prefix . "[{$key}]";
                }
                $result = $result + flatten_array($value, $key, $level + 1);
            }
            else
            {
                if ($level)
                {
                    $key = "[{$key}]";
                }
                $result[$prefix . $key] = $value;
            }
        }
    }
    return $result;
}

function form_inputs($model, $name, $fields)
{
    $CI     = & get_instance();
    $fields = explode(',', $fields);
    ob_start();
    foreach ($fields as $key => $field)
    {
        if ($field)
        {
            $group   = 'form-group';
            $data    = $class   = $input   = $check   = $control = $hidden  = NULL;
            $props   = explode(':', $field);
            $field   = array_shift($props);
            $label   = $field;
            $plural  = plural($name);

            ob_start();
            switch ($field)
            {
                case 'select':
                    $options = preg_split('/\[|\|/', preg_replace('/\]$/', '', array_shift(array_values($props))));
                    $label   = $field   = array_shift($options);
                    ?>
                    <select class="form-control" name="<?php echo $name; ?>[<?php echo $field; ?>]">
                        <?php
                        foreach ($options as $option)
                        {
                            $selected = $model->$field == $option ? 'selected' : NULL;
                            ?>
                            <option <?php echo $selected; ?> value="<?php echo $option; ?>"><?php echo t($option); ?></option>  
                            <?php
                        }
                        ?>
                    </select>
                    <?php
                    break;
//                case 'created_at':
//                    if (!$model->$field)
//                    {
//                        $hidden = form_hidden("{$name}[{$field}]", date(FORMAT_DATE_TIME));
//                    }
//                    else
//                    {
//                        $hidden = FALSE;
//                    }
//                    break;
//                case 'updated_at':
//                    $hidden   = form_hidden("{$name}[{$field}]", date(FORMAT_DATE_TIME));
//                    break;
                case 'log':
                    $label    = 'revert_changes';
                    ?>
                    <div class="collapse revert-changes in">
                        <button type="button" data-toggle="collapse" data-target=".revert-changes" class="btn btn-default btn-mini">
                            <?php echo t('access'); ?>
                        </button>
                    </div>
                    <div class="collapse revert-changes">
                        <select class="form-control" onchange="show_backup('<?php echo site_url("cms/log_{$plural}/show"); ?>', this);">
                            <option></option>
                            <?php echo objects_to_options(array_reverse($model->{"log_{$plural}"}), '{{created_at}}'); ?>
                        </select>
                    </div>
                    <?php
                    break;
                case 'important':
                case 'enabled':
                    $group    = 'checkbox';
                    $check    = form_hidden("{$name}[{$field}]", '0');
                    $check.= form_checkbox("{$name}[{$field}]", 1, set_value("{$name}[{$field}]", $model->$field) == 1);
                    break;
                case 'answer':
                case 'content':
                    $class    = 'form-control';
                    if ($ckeditor = in_array('ckeditor', $props) ? "toggle-ckeditor-{$field}" : NULL)
                    {
                        $control  = form_button(NULL, t('toogle_html_editor'), "class=\"btn btn-default btn-small\" onclick=\"CKEDITOR_toggle('{$ckeditor}');\"");
                        $ckeditor = $ckeditor ? "id=\"{$ckeditor}\" " : NULL;
                        $class.=in_array('html', $props) ? ' ckeditor' : NULL;
                        $class.=in_array('source', $props) ? ' ckeditor-instance' : NULL;
                    }
                    $input          = form_textarea("{$name}[{$field}]", set_value("{$name}[{$field}]", $model->$field), $ckeditor . "class=\"{$class}\"");
                    break;
                case 'product_colors':
                case 'pages':
                case 'product_category_id':
                case 'download_category_id':
                case 'person_category_id':
                case 'question_category_id':
                case 'template_id':
                    $describe_value = NULL;
                    if ($field == 'product_colors')
                    {
                        $describe_value = '{{color}}:{{title}}';
                    }
                    $label    = str_replace('_id', '', $field);
                    $get_all  = $CI->{ucfirst(singular($label)) . '_model'}->get_all();
                    $describe = '{{title}}';
                    $describe = $field == 'pages' ? '/{{url}}' : $describe;

                    if ($multiple = (in_array('multiple', $props) ? 'multiple size="3"' : NULL))
                    {
                        echo form_hidden("{$name}[$field][]");
                    }
                    ?>
                    <select class="form-control" name="<?php echo $name; ?>[<?php echo $field; ?>]<?php echo $multiple ? '[]' : ''; ?>" <?php echo $multiple; ?>>
                        <?php echo objects_to_options($get_all, $describe, explode(',', set_value($name . "[{$field}]", $model->$field)), in_array('null', $props), $describe_value); ?>
                    </select>
                    <?php
                    break;
                case 'image':
                case 'url':
                    $upload = in_array('upload', $props);
                    if (in_array('image', $props) OR $field == 'image')
                    {
                        $upload = TRUE;
                        $label  = 'image';
                        ?>
                        <div class="form-group"><img src="<?php echo site_url($model->$field); ?>" class="img-thumbnail"></div>
                        <?php
                    }
                    elseif (in_array('file', $props))
                    {
                        $upload = TRUE;
                        ?>
                        <div class="input-group">
                            <input class="form-control" type="text" disabled value="<?php echo $model->url; ?>">
                            <?php
                            if (isset($model->url))
                            {
                                ?>
                                <span class="input-group-btn">
                                    <?php echo anchor($model->url, t('access'), 'target="_blank" class="btn btn-default"'); ?>
                                </span>
                            <?php } ?>
                        </div>
                        <?php
                    }
                    else
                    {
                        ?>
                        <div class="input-group">
                            <?php
                            if (in_array('prefix-site-url', $props))
                            {
                                ?>
                                <span class="input-group-addon">
                                    <?php echo site_url(); ?>
                                </span>
                            <?php } ?>
                            <input class="form-control" type="text" name="<?= $name ?>[<?= $field ?>]" value="<?php echo $model->url; ?>">
                            <?php
                            if (isset($model->id))
                            {
                                ?>
                                <span class="input-group-btn">
                                    <?php echo anchor(site_url($model->url), t('access'), 'target="_blank" class="btn btn-default"'); ?>
                                </span>
                            <?php } ?>
                        </div>
                        <?php
                    }
                    if ($upload)
                    {
                        ?>
                        <input type="file" name="<?= $name ?>-<?= $field ?>" class="m-t">
                        <?php
                    }
                    break;
                default:

                    if (in_array('date', $props))
                    {
                        $class.= ' datepicker';
                    }
                    elseif (in_array('datetime', $props) OR in_array($field, array('created_at', 'updated_at')))
                    {
                        $class.= ' datetimepicker';
                        
                        if ($field == 'updated_at' OR ( $field == 'created_at' && $model->$field == '0000-00-00 00:00:00'))
                        {
                            $model->$field = date(FORMAT_DATE_TIME);
                        }
                    }
                    elseif (in_array('time', $props))
                    {
                        $class.= ' timepicker';
                    }
                    elseif (in_array('colorpicker', $props))
                    {
                        $class.= ' input-colorpicker';
                    }

                    if (in_array('textarea', $props))
                    {
                        $input = form_textarea("{$name}[{$field}]", set_value("{$name}[{$field}]", $model->$field), "class=\"form-control {$class}\" {$data}");
                    }
                    else
                    {
                        $input = form_input("{$name}[{$field}]", set_value("{$name}[{$field}]", $model->$field), "class=\"form-control {$class}\" {$data}");
                    }
            }

            if ($ob = ob_get_clean())
            {
                $input = $ob;
            }

            if ($hidden !== NULL)
            {
                echo $hidden;
            }
            else
            {
                ?>
                <div class="<?php echo $group; ?>">
                    <label> <?php echo t($check); ?> <?php echo t($label); ?> </label>
                    <?php echo $input; ?>
                    <?php
                    if ($control)
                    {
                        ?>
                        <div class="form-group m-t text-right">
                            <?php echo $control; ?>
                        </div>
                    <?php } ?>
                </div>
                <?php
            }
        }
    }
    return ob_get_clean();
}
