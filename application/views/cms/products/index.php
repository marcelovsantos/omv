<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($products as $product)
        {
        ?>
        <tr>
            <td><?php echo $product->id; ?></td>
            <td><?php echo $product->title; ?></td>
            <td>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/products/edit/{$product->id}"); ?>"><?php echo t('edit'); ?></a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/products/destroy/{$product->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                    <?php echo t('delete'); ?>
                </a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>