<div class="row">
    <div class="col-sm-8">
        <?php echo form_inputs($product, 'product', 'title,content:ckeditor:html'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo form_inputs($product, 'product', 'product_category_id:null,product_colors:null:multiple,enabled'); ?>
        <?php
        if (isset($product->id))
        {
            ?>
            <div class="form-group">
                <label><?php echo t('photos'); ?></label>
                <br>
                <?php echo anchor("cms/product_photos/index/{$product->id}", t('access'), 'class="btn btn-default"'); ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<script>
    CKFinder_options.type = 'Templates';
</script>