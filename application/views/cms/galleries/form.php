<div class="row">
    <div class="col-sm-8">
        <?php echo form_inputs($gallery, 'gallery', 'title,sequence,content:ckeditor:html'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo form_inputs($gallery, 'gallery', 'created_at,updated_at'); ?>
        <?php
        if (isset($gallery->id))
        {
            ?>
            <div class="form-group">
                <label><?php echo t('photos'); ?></label>
                <br>
                <?php echo anchor("cms/gallery_photos/index/{$gallery->id}", t('access'), 'class="btn btn-default"'); ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<script>
    CKFinder_options.type = 'Templates';
</script>