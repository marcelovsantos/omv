<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($galleries as $gallery)
        {
        ?>
        <tr>
            <td><?php echo $gallery->id; ?></td>
            <td><?php echo $gallery->title; ?></td>
            <td>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/galleries/edit/{$gallery->id}"); ?>"><?php echo t('edit'); ?></a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/galleries/destroy/{$gallery->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                    <?php echo t('delete'); ?>
                </a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>