<div class="table-responsive panel">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('login'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($users as $user)
        {
            ?>
            <tr>
                <td><?php echo $user->id; ?></td>
                <td><?php echo $user->email; ?></td>
                <td>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/users/edit/{$user->id}"); ?>"><?php echo t('edit'); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/users/destroy/{$user->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                        <?php echo t('delete'); ?>
                </a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>