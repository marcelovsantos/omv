<div class="page-header">
    <h1 id="tags">Tags</h1>
</div>
<p>
    As Tags podem ser usadas no conteúdo de Páginas e Templates.
</p>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Tag</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><code>{{site_url}}</code></td>
            <td>
                Retorna a url do site. Usado antes de qualquer link.
            </td>
        </tr>
        <tr>
            <td><code>{{base_url}}</code></td>
            <td>
                Retorna a url base do site. Usado linkar arquivos como CSS, JPG e JS.
            </td>
        </tr>
        <tr>
            <td><code>{{template_url}}</code></td>
            <td>
                Retorna a url do template. Usado para linkar os arquivos do template.
            </td>
        </tr>
        <tr>
            <td><code>{{page_title}}</code></td>
            <td>
                Retorna o título da página.
            </td>
        </tr>
        <tr>
            <td><code>{{page_content}}</code></td>
            <td>
                Retorna ao conteúdo da página.
            </td>
        </tr>
        <tr>
            <td><code>{{page_keywords}}</code></td>
            <td>
                Retorna as palavras-chave da página.
            </td>
        </tr>
        <tr>
            <td><code>{{page_description}}</code></td>
            <td>
                Retorna a descrição da página.
            </td>
        </tr>
        <tr>
            <td><code>{{piece|id}}</code></td>
            <td>
                Retorna uma parte cadastrada. Onde id deve ser o id númerico da parte.
            </td>
        </tr>
        <tr>
            <td><code>{{banner|id}}</code></td>
            <td>
                Retorna um banner cadastrado. Onde id deve ser o id númerico do banner.
                <br>Caso o id informado seja 0, serão listados todos os banners relacionados a página.
            </td>
        </tr>
        <tr>
            <td><code>{{post_value|field_name}}</code></td>
            <td>
                Retorna o valor do input no $_POST.
                <br>Utilizado para re popular um formulário com os dados enviados, ex: <code>{{post_value|email}}</code> é equivalente <code>$_POST['email']</code>.
            </td>
        </tr>
    </tbody>
</table>

<div class="page-header">
    <h1>Páginas</h1>
</div>
<p>
    Neste menu é possível: Listar, adicionar, editar e remover Páginas.
    <br> No conteúdo da página também é possível inserir Partes <small class="text-muted">(Blocos de código)</small> e Banners através de <a href="#tags">Tags</a>.
</p>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Campo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>URL</td>
            <td>
                Indica qual a url da página. <small class="text-muted">Deixar em branco para usar como página principal.</small>
            </td>
        </tr>
        <tr>
            <td>Título</td>
            <td>
                Indica o título da página para ser usado no template como: <code>&lt;title&gt;{{page_title}}&lt;/title&gt;</code>.
            </td>
        </tr>
        <tr>
            <td>Conteúdo</td>
            <td>
                Indica o conteúdo da página para ser usado no template como: <code>{{page_content}}</code>.
            </td>
        </tr>
        <tr>
            <td>Template</td>
            <td>
                Indica o template da página.
            </td>
        </tr>
        <tr>
            <td>Descrição</td>
            <td>
                Indica a descrição da página para ser usado no template como: <code>&lt;meta content="{{page_description}}" name="Description" /&gt;</code>.
            </td>
        </tr>
        <tr>
            <td>Palavras-chave</td>
            <td>
                Indica as palavras-chave da página para ser usado no template como: <code>&lt;meta content="{{page_description}}" name="Keywords" /&gt;</code>.
            </td>
        </tr>
        <tr>
            <td>Ativo</td>
            <td>
                Indica se a página está ativa para ser exibida.
            </td>
        </tr>
    </tbody>
</table>

<div class="page-header">
    <h1>Partes <small class="text-muted">(Blocos de código)</small></h1>
</div>
<p>
    Neste menu é possível: Listar, adicionar, editar e remover Partes.
    <br> As Partes são blocos de códigos, que podem ser reaproveitados em Páginas, Templates e também dentro ed outras Partes.
    <br> Para re-utilizalas basta usar a <a href="#tags">Tag</a> <code>{{piece|id}}</code>.
</p>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Campo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Título</td>
            <td>
                Indica o título da Parte.
            </td>
        </tr>
        <tr>
            <td>Conteúdo</td>
            <td>
                Indica o conteúdo da Parte.
            </td>
        </tr>
    </tbody>
</table>

<div class="page-header">
    <h1>Baners</h1>
</div>
<p>
    Neste menu é possível: Listar, adicionar, editar e remover Baners.
    <br> Para utilizar basta usar a <a href="#tags">Tag</a> <code>{{banner|id}}</code>.
</p>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Campo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Título</td>
            <td>
                Indica o título do baner.
            </td>
        </tr>
        <tr>
            <td>Imagem</td>
            <td>
                Indica a imagem do baner.
            </td>
        </tr>
        <tr>
            <td>Link</td>
            <td>
                Indica o link do baner.
            </td>
        </tr>
        <tr>
            <td>Ativo</td>
            <td>
                Indica se o baner está ativo para ser exibido.
            </td>
        </tr>
    </tbody>
</table>

<div class="page-header">
    <h1>Templates</h1>
</div>
<p>
    Neste menu é possível: Listar, adicionar, editar e remover Templates.
    <br> No conteúdo do Template também é possível inserir Partes <small class="text-muted">(Blocos de código)</small> e Banners através de <a href="#tags">Tags</a>.
</p>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Campo</th>
            <th>Descrição</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>Título</td>
            <td>
                Indica o título do template.
            </td>
        </tr>
        <tr>
            <td>Conteúdo</td>
            <td>
                Indica o conteúdo <small class="text-muted">(HTML)</small> do template .
            </td>
        </tr>
        <tr>
            <td>Arquivos</td>
            <td>
                Indica um arquivo zip com os arquivos do template <small class="text-muted">(CSS, JS, JPG)</small para fazer upload.
            </td>
        </tr>
        <tr>
            <td>Baixar Arquivos</td>
            <td>
                Faz download dos arquivos do template.
            </td>
        </tr>
    </tbody>
</table>
