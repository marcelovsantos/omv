<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('place'); ?></th>
            <th><?php echo t('date'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($suspensions as $suspension)
        {
            ?>
            <tr>
                <td><?php echo $suspension->id; ?></td>
                <td><?php echo $suspension->place; ?></td>
                <td><?php echo date(FORMAT_DATE_BR, strtotime($suspension->date)); ?></td>
                <td>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/suspensions/edit/{$suspension->id}"); ?>"><?php echo t('edit'); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/suspensions/destroy/{$suspension->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                        <?php echo t('delete'); ?>
                    </a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/suspensions/toggle/{$suspension->id}"); ?>"><?php echo t("enabled-{$suspension->enabled}"); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/suspensions/toggle/{$suspension->id}/important"); ?>"><?php echo t("important-{$suspension->important}"); ?></a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>