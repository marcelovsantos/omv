<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($download_categories as $download_category)
        {
        ?>
        <tr>
            <td><?php echo $download_category->id; ?></td>
            <td><?php echo $download_category->title; ?></td>
            <td>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/download_categories/edit/{$download_category->id}"); ?>"><?php echo t('edit'); ?></a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/download_categories/destroy/{$download_category->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                    <?php echo t('delete'); ?>
                </a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/download_categories/toggle/{$download_category->id}"); ?>"><?php echo t("enabled-{$download_category->enabled}"); ?></a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>