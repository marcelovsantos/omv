<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('date'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($flags as $flag)
        {
        ?>
        <tr>
            <td><?php echo $flag->id; ?></td>
            <td><?php echo date('m/Y', strtotime($flag->date)); ?></td>
            <td><?php echo t($flag->color); ?></td>
            <td>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/flags/edit/{$flag->id}"); ?>"><?php echo t('edit'); ?></a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/flags/destroy/{$flag->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                    <?php echo t('delete'); ?>
                </a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>