<div class="row">
    <div class="col-sm-8">
        <?php echo form_inputs($shutdown, 'shutdown', 'place,content:ckeditor:html'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo form_inputs($shutdown, 'shutdown', 'date:date,start_at:time,end_at:time,important,enabled'); ?>
    </div>
</div>