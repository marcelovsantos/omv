<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('place'); ?></th>
            <th><?php echo t('date'); ?></th>
            <th><?php echo t('start_at'); ?></th>
            <th><?php echo t('end_at'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($shutdowns as $shutdown)
        {
            ?>
            <tr>
                <td><?php echo $shutdown->id; ?></td>
                <td><?php echo $shutdown->place; ?></td>
                <td><?php echo date(FORMAT_DATE_BR, strtotime($shutdown->date)); ?></td>
                <td><?php echo date(FORMAT_TIME_BR, strtotime($shutdown->start_at)); ?></td>
                <td><?php echo date(FORMAT_TIME_BR, strtotime($shutdown->end_at)); ?></td>
                <td>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/shutdowns/edit/{$shutdown->id}"); ?>"><?php echo t('edit'); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/shutdowns/destroy/{$shutdown->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                        <?php echo t('delete'); ?>
                    </a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/shutdowns/toggle/{$shutdown->id}"); ?>"><?php echo t("enabled-{$shutdown->enabled}"); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/shutdowns/toggle/{$shutdown->id}/important"); ?>"><?php echo t("important-{$shutdown->important}"); ?></a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>