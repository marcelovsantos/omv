<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('name'); ?></th>
            <th>
                <?php echo t('options'); ?>
                <?php echo anchor('cms/subscriptions/export', t('download'), 'class="btn btn-default btn-xs"')?>
            </th>
        </tr>
        <?php
        foreach ($subscriptions as $subscription)
        {
            ?>
            <tr>
                <td><?php echo $subscription->id; ?></td>
                <td><?php echo $subscription->title; ?></td>
                <td><?php echo $subscription->email; ?></td>
                <td>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/subscriptions/edit/{$subscription->id}"); ?>"><?php echo t('edit'); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/subscriptions/destroy/{$subscription->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                        <?php echo t('delete'); ?>
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>