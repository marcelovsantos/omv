<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($person_categories as $person_category)
        {
        ?>
        <tr>
            <td><?php echo $person_category->id; ?></td>
            <td><?php echo $person_category->title; ?></td>
            <td>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/person_categories/edit/{$person_category->id}"); ?>"><?php echo t('edit'); ?></a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/person_categories/destroy/{$person_category->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                    <?php echo t('delete'); ?>
                </a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>