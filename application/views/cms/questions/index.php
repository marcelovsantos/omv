<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($questions as $question)
        {
            ?>
            <tr>
                <td><?php echo $question->id; ?></td>
                <td><?php echo $question->title; ?></td>
                <td>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/questions/edit/{$question->id}"); ?>"><?php echo t('edit'); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/questions/destroy/{$question->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                        <?php echo t('delete'); ?>
                    </a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/questions/toggle/{$question->id}"); ?>"><?php echo t("enabled-{$question->enabled}"); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/questions/toggle/{$question->id}/important"); ?>"><?php echo t("important-{$question->important}"); ?></a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>