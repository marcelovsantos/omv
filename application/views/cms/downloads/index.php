<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($downloads as $download)
        {
        ?>
        <tr>
            <td><?php echo $download->id; ?></td>
            <td><?php echo $download->title; ?></td>
            <td>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/downloads/edit/{$download->id}"); ?>"><?php echo t('edit'); ?></a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/downloads/destroy/{$download->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                    <?php echo t('delete'); ?>
                </a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/downloads/toggle/{$download->id}"); ?>"><?php echo t("enabled-{$download->enabled}"); ?></a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>