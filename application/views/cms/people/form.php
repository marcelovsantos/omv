<div class="row">
    <div class="col-sm-8">
        <?php echo form_inputs($person, 'person', 'name,email,phone,phone_secondary'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo form_inputs($person, 'person', 'person_category_id,role,url:image,select:observation[|effective|surrogate],enabled'); ?>
    </div>
</div>