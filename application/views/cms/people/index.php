<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('name'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($people as $person)
        {
        ?>
        <tr>
            <td><?php echo $person->id; ?></td>
            <td><?php echo $person->name; ?></td>
            <td>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/people/edit/{$person->id}"); ?>"><?php echo t('edit'); ?></a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/people/destroy/{$person->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                    <?php echo t('delete'); ?>
                </a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/people/toggle/{$person->id}"); ?>"><?php echo t("enabled-{$person->enabled}"); ?></a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/people/toggle/{$person->id}/important"); ?>"><?php echo t("important-{$person->important}"); ?></a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>