<div class="row">
    <?php
    foreach ($product_photos as $count => $product_photo)
    {
        ?>
        <div class="col-sm-3">
            <div class="thumbnail">
                <img src="<?php echo site_url($product_photo->url); ?>">
                <div class="caption">
                    <a class="btn btn-<?php echo $product_photo->sequence ? 'default' : 'warning'; ?> btn-xs" 
                       href="<?php echo site_url("cms/product_photos/favorite/{$product_photo->id}"); ?>"
                       style="position: absolute; top: 10px; left: 25px;"> 
                        <i class="glyphicon glyphicon-star<?php echo $product_photo->sequence ? '-empty' : ''; ?>"></i>
                    </a> 
                    <a class="btn btn-default btn-xs pull-right" 
                       href="<?php echo site_url("cms/product_photos/destroy/{$product_photo->id}"); ?>" <?php echo JS_CLICK_SURE; ?>
                       style="position: absolute; top: 10px; right: 25px;"> 
                        <i class="glyphicon glyphicon-trash"></i>
                    </a> 
                    <?php echo form_open("cms/product_photos/update/{$product_photo->id}", 'method="post"'); ?>
                    <div class="input-group">
                        <input type="text" class="form-control" name="product_photo[title]" value="<?php echo $product_photo->title; ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><?php echo t('save'); ?></button>
                        </span>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<?php echo form_open("cms/product_photos/create/{$product_id}", 'method="post" enctype="multipart/form-data" id="content-form"'); ?>
<div class="form-group">
    <label><?php echo t('new_photos'); ?></label>
    <input type="file" name="upload[]" multiple>
</div>
<?php echo form_close(); ?>