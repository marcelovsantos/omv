<div class="row">
    <div class="col-sm-8">
        <?php echo form_inputs($notice, 'notice', 'title,content:ckeditor:html'); ?>
    </div>
    <div class="col-sm-4">
        <?php // echo form_inputs($notice, 'notice', 'created_at,updated_at,author,source'); ?>
        <?php
        if (isset($notice->id))
        {
            ?>
            <div class="form-group">
                <label><?php echo t('photos'); ?></label>
                <br>
                <?php echo anchor("cms/notice_photos/index/{$notice->id}", t('access'), 'class="btn btn-default"'); ?>
            </div>
            <?php
        }
        ?>
    </div>
</div>
<script>
    CKFinder_options.type = 'Templates';
</script>