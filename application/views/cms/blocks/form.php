<div class="row">
    <div class="col-sm-<?php echo isset($block->log_blocks) ? 8 : 12; ?>">
        <?php echo form_inputs($block, 'block', 'title,content:ckeditor:source'); ?>
    </div>
    <?php
    if (isset($block->log_blocks))
    {
        ?>
        <div class="col-sm-4">
            <?php echo form_inputs($block, 'block', 'log'); ?>
        </div>
    <?php } ?>
</div>