<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($product_colors as $product_color)
        {
            ?>
            <tr>
                <td><?php echo $product_color->id; ?></td>
                <td><?php echo $product_color->title; ?></td>
                <td>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/product_colors/edit/{$product_color->id}"); ?>"><?php echo t('edit'); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/product_colors/destroy/{$product_color->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                        <?php echo t('delete'); ?>
                    </a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>