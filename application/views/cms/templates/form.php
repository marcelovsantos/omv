<div class="row">
    <div class="col-sm-<?php echo isset($template->id) ? 8 : 12; ?>">
        <?php echo form_inputs($template, 'template', 'title,content:ckeditor:source'); ?>
    </div>
    <?php
    if (isset($template->id))
    {
        ?>
        <div class="col-sm-4">
            <label><?php echo t('files'); ?></label>
            <div class="form-group">
                <?php echo anchor("cms/files/index/templates/{$template->id}", t('access'), 'class="btn btn-default" target="_blank"'); ?>
            </div>
            <?php echo form_inputs($template, 'template', 'log'); ?>
        </div>
    <?php } ?>
</div>

<script>
    var CKEDITOR_CONFIG_FULLPAGE = true;
</script>