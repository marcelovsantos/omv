<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($templates as $template)
        {
        ?>
        <tr>
            <td><?php echo $template->id; ?></td>
            <td><?php echo $template->title; ?></td>
            <td>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/templates/edit/{$template->id}"); ?>"><?php echo t('edit'); ?></a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/templates/destroy/{$template->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                    <?php echo t('delete'); ?>
                </a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>