<div class="m-t-lg wrapper-md animated fadeInUp">
    <div class="col-sm-4 col-sm-offset-4">
        <?php echo form_open('cms/sessions/create'); ?>
        <?php echo anchor('cms', t('signin'), 'class="navbar-brand block text-center m-b-lg"'); ?>
        <div class="list-group">
            <div class="list-group-item">
                <input type="text" name="" class="form-control no-border" placeholder="<?php echo t('email'); ?>" autofocus value="<?php echo set_value('session[email]'); ?>">
            </div>
            <div class="list-group-item">
                <input type="password" name="session[password]" class="form-control no-border" placeholder="<?php echo t('password'); ?>">
            </div>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit"><?php echo t('enter'); ?></button>
        <?php echo form_close(); ?>
    </div>
</div>