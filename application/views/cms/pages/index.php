<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('url'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($pages as $page)
        {
            ?>
            <tr>
                <td><?php echo $page->id; ?></td>
                <td><?php echo anchor($page->url, "/{$page->url}", 'target="_blank"'); ?></td>
                <td>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/pages/edit/{$page->id}"); ?>"><?php echo t('edit'); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/pages/destroy/{$page->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                        <?php echo t('delete'); ?>
                    </a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/pages/toggle/{$page->id}"); ?>"><?php echo t("enabled-{$page->enabled}"); ?></a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>