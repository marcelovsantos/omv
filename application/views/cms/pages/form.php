<div class="row">
    <div class="col-sm-8">
        <?php echo form_inputs($page, 'page', 'url:prefix-site-url,title,content:ckeditor:source'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo form_inputs($page, 'page', 'template_id,description,keywords,enabled,styles:textarea,scripts:textarea'); ?>
        <?php
        if (isset($page->id))
        {
            ?>
            <?php echo form_inputs($page, 'page', 'log'); ?>
        <?php } ?>
    </div>
</div>