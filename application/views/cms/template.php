<!DOCTYPE html>
<html class="app">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{page_title}}</title>
        <meta charset="utf-8">
        <link href="<?php echo base_url(); ?>assets/libs/scale/css/app.v1.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>assets/libs/colorpicker/css/colorpicker.css" rel="stylesheet">
    </head>
    <body>
        <section class="vbox">
            <?php
            if ($restrict)
            {
                ?>
                <header class="bg-dark header header-md navbar navbar-fixed-top-xs box-shadow">
                    <div class="navbar-header">
                        <button data-target="#navbar-collapse" data-toggle="collapse" class="navbar-toggle btn btn-white m-t-none" type="button">
                            <span class="glyphicon glyphicon-align-justify"></span>
                        </button>
                        <a class="navbar-brand" href="<?= site_url('cms') ?>">CMS</a>
                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <?php
                            $menus = array(
                                'cms/pages'         => 'pages',
                                'cms/notices'       => 'notices',
                                'cms/banners'       => 'banners',
                                'cms/subscriptions' => 'subscriptions',
                                'cms/files'         => 'files',
                                'cms/blocks'        => 'blocks',
                                'cms/templates'     => 'templates',
                                'cms/users'         => 'users',
                            );

                            foreach ($menus as $uri => $value)
                            {
                                if (is_array($value))
                                {
                                    ob_start();
                                    $active = FALSE;
                                    foreach ($value as $uri2 => $value2)
                                    {
                                        $active  = (( $active2 = preg_match('/^' . str_replace('/', '\/', $uri2) . '.*$/', $uri_string)) OR $active);
                                        ?>
                                        <li class="<?php echo $active2 ? 'active' : ''; ?>"><a href="<?php echo site_url($uri2); ?>"><?php echo t($value2); ?></a></li>
                                        <?php
                                    }
                                    $subs = ob_get_clean();
                                    ?>
                                    <li class="<?php echo $active ? 'active' : ''; ?> dropdown">
                                        <a href="#" data-toggle="dropdown"><?php echo t($uri); ?> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <?php echo $subs; ?>
                                        </ul>
                                    </li>
                                    <?php
                                }
                                else
                                {
                                    $active = preg_match('/^' . str_replace('/', '\/', $uri) . '.*$/', $uri_string) ? 'active' : '';
                                    ?>
                                    <li class="<?php echo $active; ?>"><a href="<?php echo site_url($uri); ?>"><?php echo t($value); ?></a></li>
                                    <?php
                                }
                            }
                            ?>
                            <li><a href="<?php echo site_url('cms/sessions/destroy') ?>" class="b"><?php echo t('exit'); ?></a></li>
                        </ul>
                    </div>
                </header>
                <?php
            }
            ?>
            <section>
                <section class="vbox">
                    <?php
                    if ($restrict)
                    {
                        $rsegments = preg_replace('/^log_/', '', $this->router->uri->rsegments);
                        $class     = preg_replace('/^log_/', '', $this->router->class);
                        $method    = preg_replace('/^log_/', '', $this->router->method);
                        $singular  = singular($class);
                        $anchor    = anchor("cms/{$class}/neew", t("new_{$singular}"), 'class="btn btn-primary btn-sm"');
                        $back      = anchor("cms/{$class}", t('back'), 'class="btn btn-default btn-sm"');
                        $save      = form_button('save', t('save'), 'class="btn btn-primary btn-sm" data-submit="#content-form"');
                        switch ($method)
                        {
                            case 'create':
                            case 'neew':
                                $header = "new_{$singular}";
                                break;
                            case 'update':
                            case 'show':
                            case 'edit':
                                $header = "edit_{$singular}";
                                break;
                            default:
                                switch ($class)
                                {
                                    case 'files':
                                        $anchor = NULL;
                                        break;
                                    default:
                                        $back   = NULL;
                                        if (!preg_match('/.+?_photo$/', $singular))
                                        {
                                            $save = NULL;
                                        }
                                        else
                                        {
                                            $anchor = NULL;
                                            $back   = anchor("cms/" . plural(preg_replace('/(.+?)_photo$/', '$1', $singular)) . "/edit/{$rsegments[3]}", t('back'), 'class="btn btn-default btn-sm"');
                                        }
                                }
                                $header = $class;
                        }
                        ?>
                        <header class="header bg-white b-b b-light">
                            <div class="row">
                                <div class="m-t-sm">
                                    <div class="col-sm-4">
                                        <span class="h4 m-t-xs inline"><?php echo t(preg_replace('/.+?_(photos?)$/', '$1', $header)); ?></span>
                                    </div>
                                    <div class="col-sm-4">
                                        <?php
                                        if ($method == 'index' && isset($search) && $search)
                                        {
                                            ?>
                                            <?php echo form_open(current_url(), 'method="get"'); ?>
                                            <div class="input-group">
                                                <?php echo form_input('search', $this->input->get('search'), 'class="form-control"') ?>
                                                <span class="input-group-btn">
                                                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                                                </span>
                                            </div>
                                            <?php echo form_close(); ?>
                                        <?php } ?>
                                    </div>
                                    <div class="col-sm-4 text-right">
                                        <?php echo "{$anchor}"; ?>
                                    </div>
                                </div>
                            </div>
                        </header>
                        <?php
                    }
                    ?>
                    <section class="scrollable wrapper <?php echo ($restrict && ($back OR $save OR isset($pagination))) ? 'w-f' : NULL ?>">
                        {{errors}}
                        <script>
                            var CKEDITOR_CONFIG_FULLPAGE = false;
                        </script>
                        <div id="content" class="m-b">
                            {{page_content}}
                        </div>
                    </section>
                    <?php
                    if ($restrict)
                    {
                        if ($back OR $save OR ( isset($pagination) && $pagination))
                        {
                            ?>
                            <footer class="footer bg-white b-t b-light text-right">
                                <?php
                                if (isset($pagination) && $pagination)
                                {
                                    echo $pagination;
                                }
                                ?>
                                <?php echo $back; ?>
                                <?php echo $save; ?>
                            </footer>

                            <?php
                        }
                    }
                    ?>
                </section>
            </section>
        </section>
        <script src="<?php echo base_url(); ?>assets/libs/jquery/jquery-2.1.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/libs/bootstrap-3.1.1-dist/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/libs/todo/js/app.v1.js"></script>
        <script src="<?php echo base_url(); ?>assets/libs/todo/js/app.plugin.js"></script>
        <?php
        if ($restrict)
        {
            ?>
            <script src="<?php echo base_url(); ?>assets/libs/bootstrap-datetimepicker/js/moment.js"></script>
            <script src="<?php echo base_url(); ?>assets/libs/bootstrap-datetimepicker/js/pt-br.js"></script>
            <script src="<?php echo base_url(); ?>assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
            <script src="<?php echo base_url(); ?>assets/libs/colorpicker/js/colorpicker.js"></script>
            <script src="<?php echo base_url(); ?>assets/libs/ckfinder/ckfinder.js"></script>
            <script src="<?php echo base_url(); ?>assets/libs/ckeditor/ckeditor.js"></script>
            <script type="text/javascript">
                                $('input.input-colorpicker').ColorPicker({
                                    onSubmit: function (hsb, hex, rgb, el) {
                                        $(el).val('#' + hex);
                                        $(el).ColorPickerHide();
                                    },
                                    onChange: function (hsb, hex, rgb) {
                                        $($(this).data('colorpicker').el).val('#' + hex);
                                    }
                                });
                                CKEDITOR.lang.load('pt-br');
                                CKEDITOR.config.codemirror = {
                                    enableCodeFormatting: true,
                                    autoFormatOnStart: true,
                                    autoFormatOnModeChange: true,
                                };
                                CKEDITOR.config.height = 300;
                                CKEDITOR.config.language = 'pt-br';
                                CKEDITOR.config.extraPlugins = 'codemirror';
                                CKEDITOR.config.fullPage = CKEDITOR_CONFIG_FULLPAGE;
                                CKEDITOR.config.allowedContent = true;
                                CKEDITOR.config.autoParagraph = false;
                                //                                CKEDITOR.config.enterMode = CKEDITOR.ENTER_BR;
                                CKEDITOR.config.forcePasteAsPlainText = false;
                                CKEDITOR.config.basicEntities = true;
                                CKEDITOR.config.entities = true;
                                CKEDITOR.config.entities_latin = false;
                                CKEDITOR.config.entities_greek = false;
                                CKEDITOR.config.entities_processNumerical = false;
                                //CKEDITOR.config.protectedSource.push(/{{(render_block[\d_]*).*?}}[\s\S]*{{\/\1}}/g);
                                //CKEDITOR.config.protectedSource.push(/{{(.*)?}}[\s\S]*{{\/\1}}/g);
                                CKEDITOR.config.protectedSource.push(/<(\w+)( +[\w\-]+=".*?")*\s*>[\s]*<\/\1>/g);
                                CKEDITOR.config.protectedSource.push(/{{block\|head}}/g);
                                CKEDITOR.dtd.$removeEmpty = {};
                                CKEDITOR.config.fillEmptyBlocks = function (element) {
                                    return true;
                                };
                                CKEDITOR.config.toolbar =
                                        [
                                            {name: 'document', items: ['Source'/*,'-','Save','NewPage','DocProps','Preview','Print','-','Templates' */]},
                                            {name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo']},
                                            /* { name: 'editing', items : [ 'Find','Replace','-','SelectAll','-','SpellChecker', 'Scayt' ] },
                                             { name: 'forms', items : [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 
                                             'HiddenField' ] },*/                         {name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize']},
                                            {name: 'colors', items: ['TextColor', 'BGColor']},
                                            {name: 'tools', items: ['Maximize', 'ShowBlocks'/*,'-','About' */]},
                                            '/',
                                            {name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat']},
                                            {name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                                                    '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']},
                                            {name: 'links', items: ['Link', 'Unlink', 'Anchor']}, {name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe']},
                                        ];

                                CKEDITOR.dtd.ul['#'] = 1;
                                CKEDITOR.dtd.ol['#'] = 1;
                                CKEDITOR.dtd.dl['#'] = 1;
                                CKEDITOR.dtd.head['#'] = 1;

                                function CKEDITOR_toggle(id) {
                                    if (CKEDITOR.instances[id]) {
                                        CKEDITOR.instances[id].destroy();
                                    } else {
                                        var options = {};
                                        if ($('#' + id).hasClass('source'))
                                        {
                                            options.startupMode = 'source';
                                        }
                                        var editor = CKEDITOR.replace(id, options);
                                        CKFinder.setupCKEditor(editor, {basePath: '../../../assets/libs/ckfinder/'}, 'Arquivos');
                                    }
                                }

                                $('.ckeditor-instance').each(function () {
                                    CKEDITOR_toggle($(this).attr('id'));
                                });

                                function show_backup(url, element) {
                                    document.location.href = url + '/' + $(element).val();
                                }

                                $('[data-submit]').click(function (e) {
                                    $($(e.currentTarget).data('submit')).get(0).submit();
                                });

                                $('.datepicker').datetimepicker({
                                    format: 'YYYY-MM-DD',
                                    pickTime: false
                                });

                                $('.datetimepicker').datetimepicker({
                                    format: 'YYYY-MM-DD HH:mm'
                                });

                                $('.timepicker').datetimepicker({
                                    format: 'HH:mm',
                                    pickDate: false
                                });
            </script>
            <?php
        }
        ?>
    </body>
</html>
i