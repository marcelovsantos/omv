<div class="row">
    <div class="col-sm-8">
        <?php echo form_inputs($product_category, 'product_category', 'title,content:ckeditor:html'); ?>
    </div>
    <div class="col-sm-4">
        <?php echo form_inputs($product_category, 'product_category', 'image,product_category_id:null,enabled'); ?>
    </div>
</div>