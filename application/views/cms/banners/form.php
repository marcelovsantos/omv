<div class="row">
    <div class="col-sm-8">
        <?php echo form_inputs($banner,'banner', 'title,url:image,sequence' );?>
    </div>
    <div class="col-sm-4">
        <?php echo form_inputs($banner,'banner', 'link,pages:multiple,start_at:datetime,end_at:datetime,enabled' );?>
    </div>
</div>