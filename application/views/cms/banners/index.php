<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('banner'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($banners as $banner)
        {
        ?>
        <tr>
            <td><?php echo $banner->id; ?></td>
            <td><img src="<?php echo site_url($banner->url); ?>" style="max-height: 30px;"></td>
            <td><?php echo $banner->title; ?></td>
            <td>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/banners/edit/{$banner->id}"); ?>"><?php echo t('edit'); ?></a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/banners/destroy/{$banner->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                    <?php echo t('delete'); ?>
                </a>
                <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/banners/toggle/{$banner->id}"); ?>"><?php echo t("enabled-{$banner->enabled}"); ?></a>
            </td>
        </tr>
        <?php
        }
        ?>
    </table>
</div>