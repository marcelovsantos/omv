<div class="row">
    <?php
    foreach ($gallery_photos as $count => $gallery_photo)
    {
        ?>
        <div class="col-sm-2">
            <div class="thumbnail">
                <img src="<?php echo site_url($gallery_photo->url); ?>">
                <div class="caption">
<!--                    <a class="btn btn-<?php echo $gallery_photo->sequence ? 'default' : 'warning'; ?> btn-xs" 
                       href="<?php echo site_url("cms/gallery_photos/favorite/{$gallery_photo->id}"); ?>"
                       style="position: absolute; top: 10px; left: 25px;"> 
                        <i class="glyphicon glyphicon-star<?php echo $gallery_photo->sequence ? '-empty' : ''; ?>"></i>
                    </a> -->
                    <a class="btn btn-default btn-xs pull-right" 
                       href="<?php echo site_url("cms/gallery_photos/destroy/{$gallery_photo->id}"); ?>" <?php echo JS_CLICK_SURE; ?>
                       style="position: absolute; top: 10px; right: 25px;"> 
                        <i class="glyphicon glyphicon-trash"></i>
                    </a> 
                    <?php echo form_open("cms/gallery_photos/update/{$gallery_photo->id}", 'method="post"'); ?>
                    <div class="input-group">
                        <input type="text" class="form-control" name="gallery_photo[sequence]" value="<?php echo $gallery_photo->sequence; ?>">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><?php echo t('save'); ?></button>
                        </span>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
        </div>
        <?php
        if ($count % 6 == 5)
        {
            ?>
        </div>
        <div class="row">
            <?php
        }
        ?>
        <?php
    }
    ?>
</div>

<?php echo form_open("cms/gallery_photos/create/{$gallery_id}", 'method="post" enctype="multipart/form-data" id="content-form"'); ?>
<div class="form-group">
    <label><?php echo t('new_photos'); ?></label>
    <input type="file" name="upload[]" multiple>
</div>
<?php echo form_close(); ?>