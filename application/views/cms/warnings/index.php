<div class="panel table-responsive">
    <table class="table table-condensed table-striped table-hover">
        <tr>
            <th><?php echo t('id'); ?></th>
            <th><?php echo t('title'); ?></th>
            <th><?php echo t('options'); ?></th>
        </tr>
        <?php
        foreach ($warnings as $warning)
        {
            ?>
            <tr>
                <td><?php echo $warning->id; ?></td>
                <td><?php echo $warning->title; ?></td>
                <td>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/warnings/edit/{$warning->id}"); ?>"><?php echo t('edit'); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/warnings/destroy/{$warning->id}"); ?>" <?php echo JS_CLICK_SURE; ?>> 
                        <?php echo t('delete'); ?>
                    </a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/warnings/toggle/{$warning->id}/enabled"); ?>"><?php echo t("enabled-{$warning->enabled}"); ?></a>
                    <a class="btn btn-default btn-xs" href="<?php echo site_url("cms/warnings/toggle/{$warning->id}/important"); ?>"><?php echo t("important-{$warning->important}"); ?></a>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
</div>