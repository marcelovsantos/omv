<nav class="navbar navbar-default" id="navbar">
    <div class="container">
        <div class="navbar-header">
            <button aria-expanded="false" class="navbar-toggle collapsed" data-target="#navbar-collapse" data-toggle="collapse" type="button"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
            </button><a class="navbar-brand" href="#"><img src="{{base_url}}assets/uploads/templates/1/images/logo.png" /> </a>
        </div>
        <div class="collapse navbar-collapse" id="navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{site_url}}quem-somos">QUEM SOMOS</a></li>
                <li>
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">SOLUÇÕES</a> 
                    <ul class="dropdown-menu">
                        <li><a href="{{site_url}}solucoes/assessoria-e-consultoria-empresarial">Assessoria e Consultoria Empresarial</a></li>
                        <li class="divider"></li>
                        <li><a href="{{site_url}}solucoes/auditoria">Auditoria</a></li>
                        <li class="divider"></li>
                        <li><a href="{{site_url}}solucoes/servicos-contabeis-especiais">Serviços Contábeis Especiais</a></li>
                    </ul>
                </li>
                <li><a href="{{site_url}}novidades-e-artigos">NOVIDADES E ARTIGOS</a></li>
                <li><a href="{{site_url}}newsletter">NEWSLETTER</a></li>
                <li><a href="{{site_url}}contato">CONTATO</a></li>
            </ul>
        </div>
    </div>
</nav>

<div id="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-2"><a href="{{site_url}}"quem-somos>QUEM SOMOS</a>
            </div>
            <div class="col-sm-2"><a href="#">SOLUÇÕES</a>
            </div>
            <div class="col-sm-2"><a href="{{site_url}}">NOVIDADES E ARTIGOS</a>
                <br />
                <a href="{{site_url}}solucoes/assessoria-e-consultoria-empresarial">Assessoria e Consultoria Empresarial</a>
                <br />
                <a href="{{site_url}}solucoes/auditoria">Auditoria</a>
                <br />
                <a href="{{site_url}}solucoes/servicos-contabeis-especiais">Serviços Contábeis Especiais</a>
            </div>
            <div class="col-sm-2"><a href="{{site_url}}newsletter">NEWSLETTER</a>
            </div>
            <div class="col-sm-2"><a href="{{site_url}}contato">CONTATO</a>
            </div>
            <div class="col-sm-2"><a href="#">FACEBOOK</a>
            </div>
        </div>
        <p class="assignature">OMV Consultoria - Todos os Direitos Reservados
        </p>
    </div>
</div>
<script src="{{base_url}}bower_components/jquery/dist/jquery.min.js">
</script>
<script src="{{base_url}}bower_components/bootstrap/dist/js/bootstrap.min.js">
</script>