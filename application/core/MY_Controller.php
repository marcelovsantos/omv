<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{

    public $restrict          = FALSE;
    public $title             = 'Cooperaliança';
    public $componentize_view = TRUE;
    public $template_path     = 'assets';
    public $data              = array();
    public $keywords, $description, $styles, $scripts;
    public $per_page          = 25;

    protected function load_page($page = NULL, $data = NULL)
    {
        $this->load->model(array('Page_model', 'Template_model'));
        $page = $page ? $page : $this->uri->uri_string();
        $page = is_int($page) ? $this->Page_model->with('template')->get($page) : $this->Page_model->with('template')->get_by(array(
                'url'     => $page,
                'enabled' => 1
        ));

        if (!$page OR ! $page->enabled)
        {
            $page = $this->Page_model->with('template')->get_by('url', '404');
        }

        $this->title        = $page->title;
        $this->view_content = $this->parser->edit_link("cms/pages/edit/{$page->id}", 'edit_page', $page->content);

        if ($data)
        {
            if (is_object($data) && get_parent_class($data) == 'Presenter')
            {
                $this->view_content = $data->parse_html($page->content);
            }
            else
            {
                $this->view_content = $this->parser->parse_string($page->content, $data, TRUE);
            }
        }

        $this->page_id          = $page->id;
        $this->keywords         = $page->keywords;
        $this->description      = $page->description;
        $this->styles           = $page->styles;
        $this->scripts          = $page->scripts;
        $this->template_content = $page->template->content;
        $this->template_path    = $this->Template_model->_upload_path . $page->template_id;
    }

    protected function flash($value, $key = 'errors')
    {
        if (!isset($this->data[$key]))
        {
            if (is_array($value))
            {
                foreach ($value as $key => $key_value)
                {
                    $value[$key] = t($key_value);
                }
            }
            else
            {
                $value = t($value);
            }

            $this->data[$key] = $value;
        }
    }

}

class CMS_Controller extends MY_Controller
{

    public $restrict          = TRUE;
    public $model_autoload    = TRUE;
    public $title             = 'CMS';
    public $componentize_view = FALSE;
    public $template          = 'cms/template';

    public function __construct()
    {

        parent::__construct();

        if ($this->restrict)
        {
            if (!$this->session->userdata('id'))
            {
                redirect('cms/sessions/neew');
            }
        }
        $this->load->helper(array('file', 'directory', 'inflector'));
        $this->data['uri_string'] = $this->uri->uri_string();
        $this->model              = singular(strtolower(get_class($this)));
        $this->model_class        = ucfirst(singular(strtolower(get_class($this)))) . '_model';
        $this->controller         = strtolower(get_class($this));

        if ($this->model_autoload)
        {
            $this->load->model($this->model_class);
        }
    }

    public function index()
    {
        $search    = $this->input->get('search');
        $search_by = $this->{$this->model_class}->_search_by;
        if ($search)
        {
            foreach ($search_by as $param)
            {
                $this->{$this->model_class}->_database->or_like($param, $search);
            }
        }
        else
        {
            $this->data['pagination'] = pagination($this->{$this->model_class}->count_all());
            $this->{$this->model_class}->limit($this->per_page, $this->input->get('page'));
        }

        $this->data['search']          = !!$search_by;
        $this->data[$this->controller] = $this->{$this->model_class}->order_by('id', 'DESC')->get_all();
    }

    public function neew()
    {
        $this->data[$this->model] = $this->{$this->model_class}->instance();
    }

    public function create()
    {
        if (${$this->model} = $this->input->post($this->model))
        {
            if ($id = $this->{$this->model_class}->insert(${$this->model}))
            {
                if ($this->_upload_and_update($id))
                {
                    redirect("cms/{$this->controller}/edit/{$id}");
                }
                else
                {
                    $this->method = 'update';
                    $this->edit($id);
                }
            }
            else
            {
                $this->flash('error_create');
                $this->neew();
            }
        }
        else
        {
            redirect("cms/{$this->controller}/neew");
        }
    }

    public function edit($id)
    {
        $this->data[$this->model] = $this->{$this->model_class}->get($id);
    }

    public function update($id)
    {
        if (${$this->model} = $this->input->post($this->model))
        {
            ${$this->model}['id'] = $id;

            if ($this->{$this->model_class}->update($id, ${$this->model}))
            {
                if ($this->_upload_and_update($id))
                {
                    redirect("cms/{$this->controller}/edit/{$id}");
                }
            }
            else
            {
                $this->flash('error_update');
                $this->edit($id);
            }
        }
        else
        {
            redirect("cms/{$this->controller}/edit/{$id}");
        }
    }

    public function destroy($id)
    {
        $this->{$this->model_class}->delete($id);
        redirect("cms/{$this->controller}");
    }

    protected function _upload_and_update($id)
    {
        $data    = array();
        $success = TRUE;
        if ($_FILES)
        {
            $allow = $this->{$this->model_class}->instance();
            foreach ($_FILES as $field => $file)
            {
                $field_name = str_replace("{$this->model}-", '', $field);
                if (property_exists($allow, $field_name))
                {
                    $upload = $this->_upload($field, $this->{$this->model_class}->_upload_path, FALSE, $this->{$this->model_class}->_upload_types);
                    if ($upload->is_valid)
                    {
                        if ($upload->data)
                        {
                            $data[$field_name] = $this->{$this->model_class}->_upload_path . $upload->data['file_name'];
                        }
                    }
                    else
                    {
                        $success = FALSE;
                        $this->flash($upload->errors);
                    }
                }
            }
        }

        if ($data)
        {
            if (!$this->{$this->model_class}->update($id, $data))
            {
                $this->flash("error_{$this->CI->router->method}");
                return FALSE;
            }
        }

        return $success;
    }

    protected function _upload($file, $_upload_path, $multi = FALSE, $allowed_types = NULL)
    {
        $data   = NULL;
        $return = (object) array(
                'is_valid' => TRUE,
                'data'     => NULL,
                'errors'   => NULL
        );

        if ($_upload_path && isset($_FILES[$file]) && $_FILES[$file]['name'])
        {
            $config['allowed_types'] = $allowed_types;
            $config['upload_path']   = $_upload_path;

            $this->load->library('upload', $config);
            if ($multi)
            {
                $this->upload->do_multi_upload($file);
                $data = $this->upload->get_multi_upload_data();
            }
            else
            {
                if ($this->upload->do_upload($file))
                {
                    $data = $this->upload->data();
                }
            }

            if ($data)
            {
                $return->data = $data;
            }
            else
            {
                $return->is_valid = FALSE;
                $return->errors   = $this->upload->display_errors();
            }
        }
        return $return;
    }

    public function toggle($id, $field = 'enabled')
    {
        if (${$this->model} = $this->{$this->model_class}->get($id))
        {
            $data = array(
                $field => 1 - ${$this->model}->$field,
                );

            $this->{$this->model_class}->update($id, $data);
        }
        $redirect = $this->input->server('HTTP_REFERER');
        $redirect = $redirect ? $redirect : "cms/{$this->controller}/index";
        redirect($redirect);
    }

}