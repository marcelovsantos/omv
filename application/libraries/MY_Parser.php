<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Parser extends CI_Parser
{

    public function __construct()
    {
        $this->CI = & get_instance();
    }

    public $l_delim          = '{{';
    public $r_delim          = '}}';
    protected $map_functions = array('set_value', 'site_url', 'base_url', 'encrypt', 'current_url', 'get_flash', 'date');
    private $map_models      = array(
        'Block'             => array('get'),
        'Download_category' => array('get_all_with_downloads', 'get_many_with_downloads'),
        'Question_category' => array('get_all_with_questions', 'get_many_with_questions'),
        'Notice'            => array('get_with_photos', 'all_pagined'),
        'Shutdown'          => array('all_pagined','all_next','calendar', 'all_by_year_and_month', 'get_five_current_months', 'all_by_year_and_month_and_day', 'current_date'),
        'Suspension'        => array('all_pagined','all_next','calendar', 'all_by_year_and_month', 'get_five_current_months', 'all_by_year_and_month_and_day', 'current_date'),
        'Banner'            => array('all_enabled'),
        'Warning'           => array('all_enabled', 'all_important', 'all_enabled'),
        'Person'            => array('all_enabled'),
        'Person_category'   => array('get_all_with_people'),
        'Question'          => array('get_all_enabled'),
        'Product_category'  => array('all_enabled'),
        'Page'              => array('get_content_by_url'),
        'Flag'              => array('get_last'),
        'Gallery'           => array('all'),
    );

    public function render_block($template, $method, $params, $data, $call)
    {
        $presenter = NULL;
        $class     = array_shift(array_values($params));
        if (count($params) == 1 && isset($data[$class]))
        {
            $presenter = $data[$class];

            if (!(is_object($presenter) && get_parent_class($presenter) == 'Presenter'))
            {
                $CI        = & get_instance();
                $presenter = $CI->presenter($class, $presenter);
            }
        }
        elseif (count($params) > 1)
        {
            $presenter = $this->call_model(array_shift($params), array_shift($params), $params);
        }

        if ($presenter)
        {
            while (preg_match("|" . preg_quote($this->l_delim) . preg_quote($call) . '.*?' . preg_quote($this->r_delim) . "(.+?)" . preg_quote($this->l_delim) . '/' . $method . preg_quote($this->r_delim) . "|s", $template, $match))
            {
                $template = str_replace($match[0], $presenter->parse_html($match[1]), $template);
            }
        }

        return $template;
    }

    public function componentize($template, $data = array())
    {
        $MAX_RECURSION = 3;
        $preg          = "/{$this->l_delim}([-\w\|,\[\]\/\@\.]+){$this->r_delim}/";

        do
        {
            preg_match_all($preg, $template, $matches);

            foreach (array_unique($matches[1]) as $key => $match)
            {
                $params = explode('|', $match);
                $method = array_shift($params);
                $value  = NULL;

                if (preg_match("/^render_block[\d_]*$/", $method) && $params)
                {
                    $template = $this->render_block($template, $method, $params, $data, $match);
                }
                elseif (array_key_exists($method, $data))
                {
                    $value = $data[$method];
                }
                elseif (method_exists($this, "_{$method}"))
                {
                    $value = @call_user_func_array(array($this, "_{$method}"), $params);
                }
                elseif (in_array($method, $this->map_functions) && function_exists($method))
                {
                    $value = @call_user_func_array($method, $params);
                }
                elseif (isset($this->map_models[$method]))
                {
                    $model  = $method;
                    if ($params && ($method = array_shift($params)))
                    {
                        $value = $this->call_model($model, $method, $params);
                    }
                }

                if ($value)
                {
                    $template = str_replace("{$this->l_delim}{$match}{$this->r_delim}", $value, $template);
                }
            }
            $MAX_RECURSION--;
        }
        while ($matches[0] && $MAX_RECURSION > 0);

        $template = preg_replace($preg, '', $template);

        return $template;
    }

    public function _block($view = NULL)
    {
        $args = func_get_args();
        return @call_user_func_array(array($this, '_render'), $args);
    }

    private function _render($view = NULL)
    {
        $path     = $template = $data     = NULL;
        $params   = func_get_args();
        array_shift($params);
        if ($params && ($model    = array_shift($params)))
        {
            if ($params && ($method = array_shift($params)))
            {
                $data = $this->call_model($model, $method, $params);
            }
        }

        if (($path = preg_replace('/^cms\//', '', $view)) && file_exists(APPPATH . "views/{$path}.php"))
        {
            $template = $this->CI->load->view($path, $data, TRUE);
        }
        else
        {
            $this->CI->load->model(array('Block_model'));

            $block = is_numeric($view) ? $this->CI->Block_model->get($view) : $this->CI->Block_model->get_by('title', $view);

            if ($block)
            {
                $template = $this->edit_link("cms/blocks/edit/{$block->id}", 'edit_block', $block->content);
            }
        }

        if ($data)
        {
            $template = $this->parse_string($template, $data, TRUE);
        }

        return $template;
    }

    public function call_model($model_name = NULL, $method = NULL, $params = array())
    {
        if (isset($this->map_models[$model_name]) && in_array($method, $this->map_models[$model_name]) && $model_name && $method)
        {
            $model = "{$model_name}_model";
            $this->CI->load->model($model);

            if (method_exists($this->CI->$model, $method))
            {
                $return = @call_user_func_array(array($this->CI->$model, $method), $params);

                if (is_scalar($return))
                {
                    return $return;
                }
                
                if(!$return)
                {
                    return new Presenter_single($this->CI->$model->_no_data);
                }

                return $this->CI->presenter->create(strtolower($model_name), $return);
            }
        }
    }

    public function edit_link($uri, $label = 'edit', $content = NULL)
    {
        if ($content && $this->CI->session->userdata('role') == 'admin' && $this->CI->input->get('edit'))
        {
            $uri = '<span class="block m-v-xs cms-live-edit">'.anchor($uri, t($label), 'class="btn btn-xs btn-default" target="_blank"').'</span>';
            return " {$content} {$uri}";
        }

        return $content;
    }

    function _parse($template, $data, $return = FALSE)
    {
        if ($template == '')
        {
            return FALSE;
        }

        if (is_object($data) && get_parent_class($data) == 'Presenter')
        {
            $template = $this->_parse_presenter($data, $template);
        }
        else
        {
            foreach ($data as $key => $val)
            {
                if (is_array($val))
                {
                    $template = $this->_parse_pair($key, $val, $template);
                }
                elseif (is_object($val) && get_parent_class($val) == 'Presenter')
                {
                    $template = $this->_parse_presenter($val, $template, $key);
                }
                else
                {
                    $template = $this->_parse_single($key, (string) $val, $template);
                }
            }
        }

        if ($return == FALSE)
        {
            $CI = & get_instance();
            $CI->output->append_output($template);
        }

        return $template;
    }

    public function _parse_presenter($presenter, $template, $key = NULL)
    {
        if ($key)
        {
            while (FALSE !== ($match = $this->_match_pair($template, $key)))
            {
                $template = str_replace($match['0'], $presenter->parse_html($match['1']), $template);
            }

            return $template;
        }

        return $presenter->parse_html($template);
    }

}