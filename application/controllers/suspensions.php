<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Suspensions extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Suspension_model'));
    }

    public function index($year = NULL, $month = NULL)
    {
        if ($this->input->is_ajax_request())
        {
            echo $this->Suspension_model->calendar($year, $month);
            exit;
        }

        $this->Suspension_model->current_year  = $year;
        $this->Suspension_model->current_month = $month;
        $this->load_page('suspensao-de-fornecimento');
    }

    public function show($year = NULL, $month = NULL, $day = NULL)
    {
        $this->Suspension_model->current_year  = $year;
        $this->Suspension_model->current_month = $month;
        $this->Suspension_model->current_day   = $day;
        if ($this->input->is_ajax_request())
        {
            $template = $this->parser->_block('SuspensionItens');
            $template = $this->parser->componentize($template);
            echo $template;
            exit;
        }
        $this->load_page('suspensao-de-fornecimento');
    }

}