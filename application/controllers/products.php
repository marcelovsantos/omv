<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Product_model', 'Product_category_model'));
    }

    public function index($id)
    {
        if ($search = $this->input->post('search'))
        {
            $this->Product_model->_database->or_like('title', $search);
            $this->Product_model->_database->or_like('content', $search);
        }
        $products = $this->Product_model->with('product_photos')->get_many_by(array(
            'enabled'             => '1',
            'product_category_id' => $id
        ));

        $products         = $this->presenter->create('product', $products);
        $product_category = $this->presenter->create('product_category', $this->Product_category_model->get($id));
        $this->load_page('categoria/produtos', array(
            'products'         => $products,
            'product_category' => $product_category
        ));
    }

    public
        function show($id)
    {
        $product                           = $this->Product_model->get_with_photos($id);
        $product                           = $this->presenter->create('product', $product);
        $this->load_page('produto', $product);
        $this->title                       = $product->title;
        $this->data['product_category_id'] = $product->product_category_id;
    }

}