<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends MY_Controller
{

    public function show()
    {
        $this->load_page();
    }

    public function home()
    {
        $this->per_page = 4;
        $this->load_page('');
    }

}