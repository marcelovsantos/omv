<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Warnings extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Warning_model'));
        $this->per_page = 5;
    }

    public function index()
    {
        $data = array(
            'warnings'   => $this->presenter->create('warning', $this->Warning_model->all_pagined()),
            'pagination' => pagination($this->Warning_model->count_by('enabled', 1))
        );
        $this->load_page('avisos', $data);
    }

    public function show($id)
    {
        $warning     = $this->Warning_model->get($id);
        $warning     = $this->presenter->create('warning', $warning);
        $this->load_page('avisos/0', $warning);
        $this->title = $warning->title;
    }

}