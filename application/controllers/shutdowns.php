<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shutdowns extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Shutdown_model'));
    }

    public function index($year = NULL, $month = NULL)
    {
        if ($this->input->is_ajax_request())
        {
            echo $this->Shutdown_model->calendar($year, $month);
            exit;
        }

        $this->Shutdown_model->current_year  = $year;
        $this->Shutdown_model->current_month = $month;
        $this->load_page('desligamentos');
    }

    public function show($year = NULL, $month = NULL, $day = NULL)
    {
        $this->Shutdown_model->current_year  = $year;
        $this->Shutdown_model->current_month = $month;
        $this->Shutdown_model->current_day   = $day;
        if ($this->input->is_ajax_request())
        {
            $template = $this->parser->_block('ShutdownItens');
            $template = $this->parser->componentize($template);
            echo $template;
            exit;
        }
        $this->load_page('desligamentos');
    }

}