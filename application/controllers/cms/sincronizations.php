<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sincronizations extends CMS_Controller
{

    public $model_autoload = FALSE;

    public function __construct()
    {
        parent::__construct();
        $this->_output                          = array();
        $this->sincronization                   = $this->load->database('sincronization', TRUE);
        $this->sincronizations                  = array();
        /**
         * Truncates
        $this->sincronizations['truncates']     = (object) array(
                'after' => array(
                    'TRUNCATE TABLE warnings',
                    'TRUNCATE TABLE people',
                    'TRUNCATE TABLE person_categories',
                    'TRUNCATE TABLE notice_photos',
                    'TRUNCATE TABLE notices',
                    'TRUNCATE TABLE shutdowns',
                    'TRUNCATE TABLE questions',
                ),
        );
         */
        /**
         * Log truncates
        $this->sincronizations['log_truncates'] = (object) array(
                'after' => array(
                    'TRUNCATE TABLE log_blocks',
                    'TRUNCATE TABLE log_pages',
                    'TRUNCATE TABLE log_templates',
                ),
        );
         */
        /**
         * Questions
        ob_start();
        ?>
        array(
        'id'        => $row->id,
        'title'      => mb_strtoupper($row->name),
        'answer'      => $row->resposta,
        'important' => (int) $row->destaque == 'Sim',
        'enabled'   => (int) $row->ativo == 'Sim',
        );
        <?php
        $this->sincronizations['questions']     = (object) array(
                'from' => 'duvidas_frequentes',
                'to'   => 'questions',
                'map'  => ob_get_clean(),
        );
         */
        /**
         * Warnings
         */
        ob_start();
        ?>
        array(
            'id'        => $row->id,
            'title'     => $row->name,
            'created_at'=> $row->created,
            'important' => (int) $row->destaque == 'Sim',
            'enabled'   => (int) $row->ativo == 'Sim',
            'content'   => str_replace(' style="margin-left: 80px;"', '', $row->corpo),
        );
        <?php
        $this->sincronizations['warnings']      = (object) array(
                'from'  => 'avisos',
                'to'    => 'warnings',
                'map'   => ob_get_clean(),
                'after' => array(
                    'UPDATE warnings AS w, 
                    (SELECT w3.id  FROM warnings w3 WHERE w3.important = 1 AND w3.id NOT IN 
                        (SELECT MAX(w4.id)  FROM warnings w4 WHERE w4.important = 1)
                    ) AS w2
                    SET w.important = 0
                    WHERE w.id = w2.id'
                ),
        );
        /**
         * Shutdowns
         */
        ob_start();
        ?>
        preg_match_all('/(\d+\D?(\d+)?)/', $row->horas, $time);
        <?php
        $pre_map                                = ob_get_clean();
        ob_start();
        ?>
        //preg_match_all('/(\d+\D?(\d+)?)/', $row->horas, $time);
        array(
        'id'        => $row->id,
        'date'      => $row->data,
        'important' => (int) $row->destaque == 'Sim',
        'enabled'   => (int) $row->ativo == 'Sim',
        'place'     => $row->locais,
        'content'   => $row->corpo,
        'start_at'  => str_replace('h', ':', @$time[0][0]),
        'end_at'    => str_replace('h', ':', @$time[0][1])
        );
        <?php
        $this->sincronizations['shutdowns']     = (object) array(
                'from'    => 'desligamentos_programados',
                'to'      => 'shutdowns',
                'map'     => ob_get_clean(),
                'pre_map' => $pre_map,
        );
        /**
         * Download_categories
         */
        ob_start();
        ?>
        array(
        'id'    => $row->id,
        'title' => $row->name
        );
        <?php
        $this->sincronizations['downloads']     = array();
        $this->sincronizations['downloads'][]   = (object) array(
                'from' => 'download_categorias',
                'to'   => 'download_categories',
                'map'  => ob_get_clean(),
        );
        /**
         * Downloads
         */
        ob_start();
        ?>
        array(
        'id'                   => $row->id,
        'title'                => $row->name,
        'created_at'           => $row->created,
        'updated_at'           => $row->modified,
        'enabled'              => (int) $row->ativo == 'Sim',
        'url'                  => '{{old_url}}downloads/download_file/' . $row->id,
        'download_category_id' => $row->download_categoria_id
        );
        <?php
        $this->sincronizations['downloads'][]   = (object) array(
                'from' => 'downloads',
                'to'   => 'downloads',
                'map'  => ob_get_clean(),
        );
        /**
         * Notices
         */
        ob_start();
        ?>
        array(
        'id'         => $row->id,
        'title'      => $row->name,
        'content'    => $row->corpo,
        'created_at' => $row->created,
        'updated_at' => $row->modified,
        'author'     => NULL,
        'source'     => $row->fonte,
        );
        <?php
        $this->sincronizations['notices']       = array();
        $this->sincronizations['notices'][]     = (object) array(
                'from' => 'noticias',
                'to'   => 'notices',
                'map'  => ob_get_clean(),
        );
        /**
         * Notice_photos
         */
        ob_start();
        ?>
        array(
        'id'        => $row->id,
        'url'       => "{{old_url}}imagens/noticias/{$row->id}.jpg",
        'notice_id' => $row->noticia_id,
        'sequence'  => 0,
        'title'     => $row->name,
        );
        <?php
        $this->sincronizations['notices'][]     = (object) array(
                'from' => 'noticia_fotos',
                'to'   => 'notice_photos',
                'map'  => ob_get_clean(),
        );
        /**
         * Person_categories
        ob_start();
        ?>
        array(
        'id'    => $row->id,
        'title' => $row->name
        );
        <?php
        $this->sincronizations['people']        = array();
        $this->sincronizations['people'][]      = (object) array(
                'from'  => 'representante_categorias',
                'to'    => 'person_categories',
                'map'   => ob_get_clean(),
                'after' =>
                array(
                    "INSERT IGNORE INTO `person_categories`(`id`, `title`)VALUES(20, 'Conselho de Administração')",
                    "INSERT IGNORE INTO `person_categories`(`id`, `title`)VALUES(21, 'Delegados FECOERUSC')"
                )
        );
         */
        /**
         * People
        ob_start();
        ?>
        array(
        'id'                 => $row->id,
        'name'               => $row->name,
        'important'          => (int) $row->destaque == 'Sim',
        'enabled'            => (int) $row->ativo == 'Sim',
        'observation'        => $row->observacao == 'Suplente' ? 'surrogate' : ($row->observacao == 'Efetivo' ? 'effective' : NULL),
        'phone'              => $row->fone1,
        'phone_secondary'    => $row->fone2,
        'email'              => $row->email,
        'url'                => '',
        'person_category_id' => $row->representante_categoria_id,
        'role'               => '',
        );
        <?php
        $this->sincronizations['people'][]      = (object) array(
                'from'  => 'representantes',
                'to'    => 'people',
                'map'   => ob_get_clean(),
                'after' => array(
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `person_category_id`, `role`) VALUES(50, 'Pedro Deonízio Gabriel ', 1, 1, 20, 'Presidente')",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `person_category_id`, `role`) VALUES(51, 'Jorge Rodrigues ', 1, 1, 20, 'Vice-Presidente')",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `person_category_id`, `role`) VALUES(52, 'Severiano Antonio Valentim ', 1, 1, 20, 'Secretário')",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `person_category_id`, `role`) VALUES(53, 'Juscelino Dagostin ', 1, 1, 20, 'Conselheiro')",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `person_category_id`, `role`) VALUES(54, 'Pedro Luiz da Luz ', 1, 1, 20, 'Conselheiro')",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `person_category_id`, `role`) VALUES(55, 'Hilário Dal Molin ', 1, 1, 20, 'Conselheiro')",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `person_category_id`, `role`) VALUES(56, 'Silvio João Viana ', 1, 1, 20, 'Conselheiro')",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `person_category_id`, `role`) VALUES(57, 'Cláudio Blissari ', 1, 1, 20, 'Conselheiro')",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `observation`, `person_category_id`) VALUES(58, 'Everaldo Borges Réus ', 1, 1, 'effective', 21)",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `observation`, `person_category_id`) VALUES(59, 'Quintino Pavei ', 1, 1, 'effective', 21)",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `observation`, `person_category_id`) VALUES(60, 'Wagner Pereira Pizzetti ', 1, 1, 'surrogate', 21)",
                    "INSERT IGNORE INTO `people`(`id`, `name`, `important`, `enabled`, `observation`, `person_category_id`) VALUES(61, 'Evanir Calegari ', 1, 1, 'surrogate', 21)",
                )
        );
         */
    }

    public function index()
    {
        foreach ($this->sincronizations as $key => $sincronization)
        {
            //$this->show($key);
        }
    }

    public function show($id = NULL)
    {
        $output = '';
        if (isset($this->sincronizations[$id]))
        {
            $sincronizations = $this->sincronizations[$id];
            if (!is_array($sincronizations))
            {
                $sincronizations = array($sincronizations);
            }
            foreach ($sincronizations as $sincronization)
            {
                if (isset($sincronization->from) && isset($sincronization->to) && isset($sincronization->map))
                {
                    $output .= "<h1>{$sincronization->from} to {$sincronization->to}</h1><p class=\"text-xs\">";
                    $query = $this->sincronization->get($sincronization->from);
                    foreach ($query->result() as $row)
                    {
                        $sql = array();
                        if (isset($sincronization->pre_map))
                        {
                            eval($sincronization->pre_map);
                        }
                        eval("\$map_php = {$sincronization->map}");
                        foreach ($map_php as $key => $value)
                        {
                            $sql[] = "{$key} = ?";
                        }
                        $values = array_values($map_php);
                        $sql    = "REPLACE INTO {$sincronization->to} SET " . implode(', ', $sql);
                        $status = $this->db->query($sql, array_values($map_php));
                        $status = $status ? 'ok' : 'remove';
                        $output .= "<span class=\"glyphicon glyphicon-{$status}\"></span> " . array_shift($values) . " | ";
                    }
                }

                if (isset($sincronization->after))
                {
                    $after = $sincronization->after;
                    if (!is_array($after))
                    {
                        $after = array($after);
                    }
                    foreach ($after as $sql)
                    {
                        $status = $this->db->query($sql);
                        $status = $status ? 'ok' : 'remove';
                        $output .= "</p><p style=\"white-space: pre;\"><span class=\"glyphicon glyphicon-{$status}\"></span> " . htmlentities($sql);
                    }
                }

                $output .= "</p>";
            }
            $this->_output[]      = $output;
            $this->view           = 'cms/sincronizations/index';
            $this->data['output'] = $this->_output;
        }
    }

}