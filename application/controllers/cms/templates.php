<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Templates extends CMS_Controller
{

    public function get($id)
    {
        parent::with('log_templates');
        return parent::get($id);
    }

}