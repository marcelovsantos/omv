<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class People extends CMS_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Person_category_model'));
    }

}