<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Log_templates extends CMS_Controller
{

    public function show($id)
    {
        $this->load->model(array('Log_template_model', 'Template_model'));
        $log                    = $this->Log_template_model->get($id);
        $template               = $this->Template_model->with('log_templates')->get($log->template_id);
        $template->content      = $log->content;
        $this->data['template'] = $template;
        $this->view             = 'cms/templates/edit';
    }

}