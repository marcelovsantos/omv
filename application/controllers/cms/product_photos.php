<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_photos extends CMS_Controller
{
    public function index($product_id)
    {
        $this->data['product_photos']    = $this->Product_photo_model->get_many_by('product_id', $product_id);
        $this->data['product_id'] = $product_id;
    }

    public function create($product_id)
    {
        $_upload_path   = $this->Product_photo_model->_upload_path . $product_id;
        $upload = $this->_upload('upload', $_upload_path, TRUE, $this->Product_photo_model->_upload_types);

        if ($upload->is_valid)
        {
            if ($upload->data)
            {
                foreach ($upload->data as $data)
                {
                    $product_photo              = array();
                    $product_photo['url']       = $_upload_path . '/' . $data['file_name'];
                    $product_photo['product_id'] = $product_id;
                    $this->Product_photo_model->insert($product_photo);
                }
                redirect("cms/product_photos/index/{$product_id}");
            }
        }
        else{
            $this->flash($upload->errors);
        }

        $this->index($product_id);
        $this->view = "cms/product_photos/index";
    }

    public function update($id)
    {
        $product_photo = $this->Product_photo_model->get($id);

        if ($post = $this->input->post('product_photo'))
        {
            if ($this->Product_photo_model->update($id, $post))
            {
                redirect("cms/product_photos/index/{$product_photo->product_id}");
            }
        }
        $this->flash('error_update');
        $this->view = "cms/product_photos/index";
        $this->index();
    }

    public function destroy($id)
    {
        $this->load->helper('file');
        $product_photo     = $this->Product_photo_model->get($id);
        $product_id = $product_photo->product_id;
        $this->Product_photo_model->delete($id);
        redirect("cms/product_photos/index/{$product_id}");
    }

    public function favorite($id)
    {
        $product_photo = $this->Product_photo_model->get($id);
        $this->Product_photo_model->favorite($product_photo);
        redirect("cms/product_photos/index/{$product_photo->product_id}");
    }

}