<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_photos extends CMS_Controller
{

    public function index($gallery_id)
    {
        $this->data['gallery_photos'] = $this->Gallery_photo_model->get_many_by('gallery_id', $gallery_id);
        $this->data['gallery_id']     = $gallery_id;
    }

    public function create($gallery_id)
    {
        $_upload_path = $this->Gallery_photo_model->_upload_path . $gallery_id;
        $upload       = $this->_upload('upload', $_upload_path, TRUE, $this->Gallery_photo_model->_upload_types);

        if ($upload->is_valid)
        {
            if ($upload->data)
            {
                foreach ($upload->data as $data)
                {
                    $gallery_photo               = array();
                    $gallery_photo['url']        = $_upload_path . '/' . $data['file_name'];
                    $gallery_photo['gallery_id'] = $gallery_id;
                    $this->Gallery_photo_model->insert($gallery_photo);
                }
                redirect("cms/gallery_photos/index/{$gallery_id}");
            }
        }
        else
        {
            $this->flash($upload->errors);
        }

        $this->index($gallery_id);
        $this->view = "cms/gallery_photos/index";
    }

    public function update($id)
    {
        $gallery_photo = $this->Gallery_photo_model->get($id);

        if ($post = $this->input->post('gallery_photo'))
        {
            if ($this->Gallery_photo_model->update($id, $post))
            {
                redirect("cms/gallery_photos/index/{$gallery_photo->gallery_id}");
            }
        }
        $this->flash('error_update');
        $this->view = "cms/gallery_photos/index";
        $this->index();
    }

    public function destroy($id)
    {
        $this->load->helper('file');
        $gallery_photo = $this->Gallery_photo_model->get($id);
        $gallery_id    = $gallery_photo->gallery_id;
        $this->Gallery_photo_model->delete($id);
        redirect("cms/gallery_photos/index/{$gallery_id}");
    }

    public function favorite($id)
    {
        $gallery_photo = $this->Gallery_photo_model->get($id);
        $this->Gallery_photo_model->favorite($gallery_photo);
        redirect("cms/gallery_photos/index/{$gallery_photo->gallery_id}");
    }

}