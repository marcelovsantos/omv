<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Questions extends CMS_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Question_category_model'));
    }

}