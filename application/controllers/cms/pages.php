<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages extends CMS_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array( 'Template_model'));
    }
}