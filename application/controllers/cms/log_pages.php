<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Log_pages extends CMS_Controller
{

    public function show($id)
    {
        $this->load->model(array('Log_page_model', 'Page_model', 'Template_model'));
        $log                = $this->Log_page_model->get($id);
        $page               = $this->Page_model->with('log_pages')->get($log->page_id);
        $page->content      = $log->content;
        $page->scripts      = $log->scripts;
        $page->styles       = $log->styles;
        $this->data['page'] = $page;
        $this->view         = 'cms/pages/edit';
    }

}