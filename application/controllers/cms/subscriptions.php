<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Subscriptions extends CMS_Controller
{

    public function export()
    {
        $query = $this->Subscription_model->_database->select('GROUP_CONCAT(CONCAT("\'", title,"\' <", email, ">")) AS export', FALSE)->get('subscriptions');
        $row   = $query->row();

        $this->load->helper('download');
        force_download('subscriptions.txt', $row->export);
        exit;
    }

}