<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notice_photos extends CMS_Controller
{
    public function index($notice_id)
    {
        $this->data['notice_photos']    = $this->Notice_photo_model->get_many_by('notice_id', $notice_id);
        $this->data['notice_id'] = $notice_id;
    }

    public function create($notice_id)
    {
        $_upload_path   = $this->Notice_photo_model->_upload_path . $notice_id;
        $upload = $this->_upload('upload', $_upload_path, TRUE, $this->Notice_photo_model->_upload_types);

        if ($upload->is_valid)
        {
            if ($upload->data)
            {
                foreach ($upload->data as $data)
                {
                    $notice_photo              = array();
                    $notice_photo['url']       = $_upload_path . '/' . $data['file_name'];
                    $notice_photo['notice_id'] = $notice_id;
                    $this->Notice_photo_model->insert($notice_photo);
                }
                redirect("cms/notice_photos/index/{$notice_id}");
            }
        }
        else{
            $this->flash($upload->errors);
        }

        $this->index($notice_id);
        $this->view = "cms/notice_photos/index";
    }

    public function update($id)
    {
        $notice_photo = $this->Notice_photo_model->get($id);

        if ($post = $this->input->post('notice_photo'))
        {
            if ($this->Notice_photo_model->update($id, $post))
            {
                redirect("cms/notice_photos/index/{$notice_photo->notice_id}");
            }
        }
        $this->flash('error_update');
        $this->view = "cms/notice_photos/index";
        $this->index();
    }

    public function destroy($id)
    {
        $this->load->helper('file');
        $notice_photo     = $this->Notice_photo_model->get($id);
        $notice_id = $notice_photo->notice_id;
        $this->Notice_photo_model->delete($id);
        redirect("cms/notice_photos/index/{$notice_id}");
    }

    public function favorite($id)
    {
        $notice_photo = $this->Notice_photo_model->get($id);
        $this->Notice_photo_model->favorite($notice_photo);
        redirect("cms/notice_photos/index/{$notice_photo->notice_id}");
    }

}