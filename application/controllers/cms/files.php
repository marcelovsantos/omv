<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');
include_once BASEPATH . '../assets/libs/ckfinder/ckfinder.php';

class Files extends CMS_Controller
{
    public $model_autoload;

    public function index($resources = NULL, $id = NULL, $popup = 0)
    {
        $finder                 = new CKFinder();
        $finder->BasePath       = base_url().'assets/libs/ckfinder/';
        $finder->SelectFunction = 'ShowFileInfo';
        $finder->Width          = '100%';
        $finder->Height         = '600';
        
        if ($popup)
        {
            $this->template_content = '{{page_content}}';
        }
        
        switch ($resources)
        {
            case 'templates':
                $finder->ResourceType = 'Templates';
                if ($id)
                {
                    $finder->StartupPath = "Templates:/{$id}/:1";
                }
                break;
            case 'news':
                $finder->ResourceType = 'Notícias';
                if ($id)
                {
                    $finder->StartupPath = "Notícias:/{$id}/:1";
                }
                break;
            default:
                $finder->ResourceType = 'Arquivos';
        }
        $this->data['finder'] = $finder;
    }

}