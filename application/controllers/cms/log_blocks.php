<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Log_blocks extends CMS_Controller
{

    public function show($id)
    {
        $this->load->model(array('Log_block_model', 'Block_model'));
        $log                 = $this->Log_block_model->get($id);
        $block               = $this->Block_model->with('log_blocks')->get($log->block_id);
        $block->content      = $log->content;
        $this->data['block'] = $block;
        $this->view          = 'cms/blocks/edit';
    }

}