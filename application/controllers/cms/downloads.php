<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Downloads extends CMS_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Download_category_model'));
    }

}