<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Products extends CMS_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Product_category_model', 'Product_color_model'));
    }

}