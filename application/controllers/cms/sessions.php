<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sessions extends CMS_Controller
{

    public $restrict = FALSE;
    public $model_autoload = FALSE;

    public function neew()
    {
        
    }

    public function create()
    {
        $this->load->model('User_model');
        $session             = $this->input->post('session');
        $session['password'] = md5($session['password']);

        if ($session = $this->User_model->get_by($session))
        {
            unset($session->password);
            $this->session->set_userdata($session);
            redirect('cms/pages');
        }

        $this->neew();
        $this->flash('error_signin');
    }

    public function destroy()
    {
        $this->session->sess_destroy();
        redirect('cms');
    }

}