<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Downloads extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Download_model'));
    }

    public function show($id)
    {
        $this->load->helper('download');
        $download = $this->Download_model->get_enabled($id);

        if (strpos($this->Download_model->_upload_path, $download->url) === 0)
        {
            force_download(preg_replace('/^.*\/(.+)$/', '$1', $download->url), file_get_contents($download->url));
        }
        else
        {
            redirect(site_url($download->url));
        }
        
        exit;
    }

}