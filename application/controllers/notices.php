<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notices extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model(array('Notice_model'));
    }

    public function show($id = NULL)
    {
        $notice      = $this->Notice_model->get_with_photos($id);
        $notice      = $this->presenter->create('notice', $notice);
        $this->load_page('novidade', $notice);
        $this->title = $notice->title;
    }

}