<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Subscriptions extends MY_Controller
{

    public function create()
    {
        $this->load->model('Subscription_model');

        $flash        = 'Não foi possível continuar.';
        $status       = 'danger';
        $subscription = $this->input->post('subscription');

        if ($model = $this->Subscription_model->get_by('email', $subscription['email']))
        {
            $this->Subscription_model->delete($model->id);
            $flash  = 'E-mail removido da lista.';
            $status = 'success';
        }
        elseif ($subscription)
        {
            $this->Subscription_model->insert($subscription);
            $flash  = 'E-mail inserido na lista.';
            $status = 'success';
        }

        redirect(site_url("newsletter?flash[{$status}]={$flash}"));
    }

}