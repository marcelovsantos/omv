<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contacts extends MY_Controller
{

    public function create()
    {
        if ($post = $this->input->post())
        {

            $this->load->library('encrypt');
            $sender  = "marcelovsantos@gmail.com";
            $to      = $this->encrypt->decode($post['to']);
            $reply   = $post['email'];
            $subject = $post['subject'];
            $message = '';
            $headers = implode("\n", array("From: {$sender}", "Reply-To: {$reply}", "Subject: {$subject}", "Return-Path:  {$sender}", "MIME-Version: 1.0", "X-Priority: 3", "Content-Type: text/html; charset=UTF-8"));
            
            $post_body = $post;
            unset($post_body['to']);
            unset($post_body['subject']);
            unset($post_body['continue']);
            
            foreach ($post_body as $key => $value)
            {
                $message.= "<strong>{$key}:</strong> {$value}<br><br>";
            }

//            $this->load_page();

            if (mail($to, $subject, $message, $headers))
            {
                $flash = '?flash[success]=Seu e-mail foi enviado com sucesso';
            }
            else
            {
                $flash = '?flash[danger]=Não foi possível enviar seu e-mail tente novamente mais tarde.';
            }
            redirect("{$post['continue']}{$flash}");
        }
    }

}