<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_category_model extends MY_model
{

    public $_upload_path  = 'assets/uploads/product_category_photos/';
    public $_upload_types = 'gif|jpg|png';
    public $before_delete = array('delete_file');
    public $has_many      = array('products', 'product_categories');

    protected function delete_file($product_category)
    {
        @unlink($product_category['image']);
        return $product_category;
    }

    public function all_enabled()
    {
        return $this->with('product_categories')->get_many_by('enabled', '1');
    }

    public function get_with_products($id)
    {
        return $this->with('products')->get($id);
    }

}
