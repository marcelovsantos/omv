<?php

class User_model extends MY_Model
{

    public $before_update = array('md5_password');
    public $before_create = array('md5_password');

    protected function md5_password($user)
    {
        if ($user['password'])
        {
            $user['password'] = md5($user['password']);
        }
        else
        {
            unset($user['password']);
        }
        return $user;
    }

}