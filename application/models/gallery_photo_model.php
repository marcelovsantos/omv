<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_photo_model extends MY_Model
{

    public $_search_by    = array();
    public $_upload_path  = 'assets/uploads/gallery_photos/';
    public $_upload_types = 'gif|jpg|png';

    public function favorite($gallery_photo)
    {
        $this->_database->query('UPDATE gallery_photos SET  `sequence` = IF(id = ?, 0, 1) WHERE gallery_id = ?', array($gallery_photo->id, $gallery_photo->gallery_id));
    }

    public function delete_photos($gallery_id)
    {
        $gallery_photos = $this->get_many_by('gallery_id', $gallery_id);
        $ids            = array();

        foreach ($gallery_photos as $gallery_photo)
        {
            $ids[] = $gallery_photo->id;
            @unlink($gallery_photo->url);
        }

        if ($ids)
        {
            $this->delete_many($ids);
        }
    }

    public function delete_photo($gallery_photo)
    {
        $url = $this->get($gallery_photo);
        @unlink($url->url);
        return $gallery_photo;
    }

    public function get_many_by($arg1, $arg2)
    {
        $this->order_by('sequence');
        return parent::get_many_by($arg1, $arg2);
    }

}