<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Gallery_model extends MY_Model
{

    public $has_many      = array('gallery_photos');
    public $after_create  = array('create_dir');
    public $before_delete = array('delete_dir');

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Gallery_photo_model');
    }

    public function all()
    {
        $galleries = $this->order_by('sequence');
        $galleries->with('gallery_photos');
        $galleries = $galleries->get_all();
        return $galleries;
    }

    public function get_with_photos($id)
    {
        return $this->with('gallery_photos')->get($id);
    }

    public function create_dir($gallery)
    {
        mkdir($this->Gallery_photo_model->_upload_path . $gallery);
        return $gallery;
    }

    public function delete_dir($gallery)
    {
        $this->Gallery_photo_model->delete_photos($gallery);
        rmdir($this->Gallery_photo_model->_upload_path . $gallery);
        return $gallery;
    }

}