<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_photo_model extends MY_Model
{
    public $_search_by = array();
    public $_upload_path  = 'assets/uploads/product_photos/';
    public $_upload_types = 'gif|jpg|png';

    public function favorite($product_photo)
    {
        $this->_database->query('UPDATE product_photos SET  `sequence` = IF(id = ?, 0, 1) WHERE product_id = ?', array($product_photo->id, $product_photo->product_id));
    }

    public function delete_photos($product_id)
    {
        $product_photos = $this->get_many_by('product_id', $product_id);
        $ids    = array();

        foreach ($product_photos as $product_photo)
        {
            $ids[] = $product_photo->id;
            @unlink($product_photo->url);
        }

        if ($ids)
        {
            $this->delete_many($ids);
        }
    }

    public function delete_photo($product_photo)
    {
        $url = $this->get($product_photo);
        @unlink($url->url);
        return $product_photo;
    }

}