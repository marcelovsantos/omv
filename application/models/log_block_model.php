<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Log_block_model extends MY_Model
{

    public $before_create = array('created_timestamp');

    protected function created_timestamp($log)
    {
        $log['created_at'] = date(FORMAT_DATE_TIME);
        return $log;
    }

}