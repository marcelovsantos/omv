<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Warning_model extends MY_Model
{

    public function all_pagined($per_page = NULL, $offset = NULL)
    {
        $warnings = $this->limit($per_page ? $per_page : $this->per_page, $offset ? $offset : $this->input->get('page'));
        $warnings = $warnings->order_by(array('important' => 'DESC', 'id' => 'DESC'))->get_many_by(array(
            'enabled' => '1',
        ));
        return $warnings;
    }

    public function all_important()
    {
        return $this->get_many_by(array(
                'enabled'   => '1',
                'important' => '1',
        ));
    }

    public function all_enabled()
    {
        return $this->order_by(array('important' => 'DESC', 'id' => 'DESC'))->get_many_by(array(
                'enabled' => '1',
        ));
    }

}