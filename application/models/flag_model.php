<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Flag_model extends MY_Model
{

    public function get_last()
    {
        return $this->order_by('date', 'DESC')->limit(1)->get_all();
    }

}