<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Person_model extends MY_model
{

    public $_search_by = array('name');
    public $_upload_path  = 'assets/uploads/people/';
    public $_upload_types = 'gif|jpg|png';
    public $belongs_to    = array('person_category');
    public $before_delete = array('delete_file');

    public function all_enabled()
    {
        return $this->get_many_by('enabled', '1');
    }

    protected function delete_file($person)
    {
        @unlink($person['url']);
        return $person;
    }

}