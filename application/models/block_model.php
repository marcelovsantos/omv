<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Block_model extends MY_model
{

    public $_search_by    = array('title', 'content');
    public $has_many      = array('log_blocks');
    public $_upload_path  = 'assets/uploads/blocks/';
    public $before_update = array('create_log');

    public function get($id)
    {
        parent::with('log_blocks');
        return parent::get($id);
    }

    public function create_log($block)
    {
        $this->load->model('Log_block_model');
        $backup = $this->get($block['id']);
        $this->Log_block_model->insert(array(
            'block_id' => $backup->id,
            'content'  => $backup->content
        ));
        return $block;
    }

}