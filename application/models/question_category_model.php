<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Question_category_model extends MY_model
{

    public $has_many = array('questions');

    public function get_all_with_questions()
    {
        return $this->with('questions')->get_all();
    }

    public function get_many_with_questions($ids)
    {
        $this->load->model('question_model');
        $question_categories = $this->get_many_by('id', explode(',', $ids));
        foreach ($question_categories as $question_category)
        {
            $questions                    = new Question_model();
            $question_category->questions = $questions->order_by('created_at', 'DESC')->get_many_by(array(
                'question_category_id' => $question_category->id
                )
            );
        }
        return $question_categories;
    }

}