<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Page_model extends MY_model
{

    public $_search_by    = array('title', 'url', 'content');
    public $belongs_to    = array('template');
    public $has_many      = array('log_pages');
    public $before_update = array('create_log');

    public function get($id)
    {
        parent::with('log_pages');
        return parent::get($id);
    }

    public function get_content_by_url($url)
    {
        return $this->get_by(array('url' => $url))->content;
    }

    public function create_log($page)
    {
        $this->load->model('Log_page_model');
        $backup = $this->get($page['id']);
        $this->Log_page_model->insert(array(
            'page_id' => $backup->id,
            'content' => $backup->content,
            'scripts' => $backup->scripts,
            'styles'  => $backup->styles
        ));
        return $page;
    }

}