<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notice_model extends MY_Model
{

    public $has_many      = array('notice_photos');
    public $after_create  = array('create_dir');
    public $before_delete = array('delete_dir');

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Notice_photo_model');
    }

    public function all_pagined($per_page = NULL, $offset = NULL)
    {
        $notices = $this->order_by('id', 'DESC')
            ->limit($per_page ? $per_page : $this->per_page, $offset ? $offset : $this->input->get('page'));

        $notices->with('notice_photos');
        $notices = $notices->get_all();
        return $notices;
    }

    public function get_with_photos($id)
    {
        return $this->with('notice_photos')->get($id);
    }

    public function create_dir($notice)
    {
        mkdir($this->Notice_photo_model->_upload_path . $notice);
        return $notice;
    }

    public function delete_dir($notice)
    {
        $this->Notice_photo_model->delete_photos($notice);
        rmdir($this->Notice_photo_model->_upload_path . $notice);
        return $notice;
    }

}