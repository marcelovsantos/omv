<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Shutdown_model extends MY_Model
{

    public $_no_data = '<p>Não há desligamentos programados para esta data.</p>';
    public $current_day, $current_month, $current_year;

    public function all_pagined($per_page = NULL, $offset = NULL)
    {
        return $this->order_by(
                    array(
                        'date'      => 'DESC',
                        'important' => 'DESC'
                ))
                ->limit($per_page ? $per_page : $this->per_page, $offset ? $offset : $this->input->get('page'))
                ->as_object()
                ->get_many_by(array(
                    'enabled' => '1',
                    //'date >=' => date('Y-m-d')
        ));
    }

    public function all_next($per_page = NULL, $offset = NULL)
    {
        return $this->order_by(
                    array(
                        'date'      => 'ASC',
                        'important' => 'DESC'
                ))
                ->limit($per_page ? $per_page : $this->per_page, $offset ? $offset : $this->input->get('page'))
                ->as_object()
                ->get_many_by(array(
                    'enabled' => '1',
                    'date >=' => date('Y-m-d')
        ));
    }

    public function all_by_year_and_month($year = NULL, $month = NULL)
    {
        $month     = $this->month($month);
        $year      = $this->year($year);
        $reference = gmmktime(1, 1, 1, $month, 1, $year);
        $start     = date('Y-m-01', $reference);
        $end       = date('Y-m-t', $reference);
        $return    = $this->order_by(array('date' => 'ASC', 'important' => 'DESC'))
            ->as_object()
            ->get_many_by(array(
            'enabled' => '1',
            'date >=' => $start,
            'date <=' => $end,
        ));
        return $return;
    }

    public function all_by_year_and_month_and_day($year = NULL, $month = NULL, $day = NULL)
    {
        $day       = $this->day($day) + 1;
        $month     = $this->month($month) - 1;
        $year      = $this->year($year);
        $reference = gmmktime(1, 1, 1, $month, $day, $year);
        $reference = date('Y-m-d', $reference);

        $return = $this->order_by('start_at', 'ASC')
            ->as_object()
            ->get_many_by(array(
            'enabled' => '1',
            'date'    => $reference
        ));
        return $return;
    }

    public function get_five_current_months($year = NULL, $month = NULL)
    {
        $month  = $this->month($month);
        $year   = $this->year($year);
        $months = array(
            gmmktime(1, 1, 1, $month - 1, 1, $year),
            gmmktime(1, 1, 1, $month, 1, $year),
            gmmktime(1, 1, 1, $month + 1, 1, $year),
            gmmktime(1, 1, 1, $month + 2, 1, $year),
            gmmktime(1, 1, 1, $month + 3, 1, $year),
        );

        foreach ($months as $key => $time)
        {
            $months[$key] = (object) array(
                    'url'   => date('Y/m', $time),
                    'label' => mb_strtoupper(month_to_br(date('M', $time))) . '/' . date('Y', $time),
                    'class' => '',
            );
        }

        $months[0]->label                  = '&#10094;';
        $months[1]->class                  = 'active';
        $months[count($months) - 1]->label = '&#10095;';
        return $months;
    }

    public function calendar($year = NULL, $month = NULL)
    {
        $month = $this->month($month) - 1;
        $year  = $this->year($year);
        $prefs = array(
            'template'       => '
                {heading_title_cell}<th colspan="{colspan}" class="text-center"><span class="text-nm">{heading}</span></th>{/heading_title_cell}
                {table_open}<table class="table bg-light b-ns text-center text-sm table-condensed" id="calendar">{/table_open}
                {cal_cell_no_content}<span>{day}</span>{/cal_cell_no_content}
                {cal_cell_no_content_today}<span class="active">{day}</span>{/cal_cell_no_content_today}
                {cal_cell_content}<a data-trigger="trigger_carousel" data-ajax="#calendar-show" href="{content}/{day}">{day}</a>{/cal_cell_content}
                {cal_cell_content_today}<a data-trigger="trigger_carousel" data-ajax="#calendar-show" class="active" href="{content}/{day}">{day}</a>{/cal_cell_content_today}
                {week_row_start}{/week_row_start}
                {week_day_cell}{/week_day_cell}
                {week_row_end}{/week_row_end}
                {heading_previous_cell}<th><a class="text-color-nm text-right" data-ajax="#calendar" href="{previous_url}">&lt;</a></th>{/heading_previous_cell}
                {heading_next_cell}<th><a class="text-color-nm text-left" data-ajax="#calendar" href="{next_url}">&gt;</a></th>{/heading_next_cell}
                ',
            'show_next_prev' => TRUE,
            'next_prev_url'  => 'desligamentos'
        );

        $data = array();

        $url = site_url("desligamentos/{$year}/{$month}");

        $shutdowns = $this->all_by_year_and_month($year, $month);
        $data      = array();
        foreach ($shutdowns as $shutdown)
        {
            $day = (int) substr($shutdown->date, 8);
            if (!isset($data[$day]))
            {
                $data[$day] = $url;
            }
        }

        $this->load->library('calendar', $prefs);
        return $this->calendar->generate($year, $month, $data);
    }

    public function current_date($year = NULL, $month = NULL, $day = NULL)
    {
        $day       = $this->day($day) + 1;
        $month     = $this->month($month) - 1;
        $year      = $this->year($year);
        $reference = gmmktime(1, 1, 1, $month, $day, $year);
        $reference = date('Y-m-d', $reference);
        return date(FORMAT_DATE_BR, strtotime($reference));
    }

    private function month($month = NULL)
    {
        return ($month ? $month : ($this->Shutdown_model->current_month ? $this->Shutdown_model->current_month : date('m'))) + 1;
    }

    private function year($year = NULL)
    {
        return $year ? $year : ($this->Shutdown_model->current_year ? $this->Shutdown_model->current_year : date('Y'));
    }

    private function day($day = NULL)
    {
        return $day ? $day : ($this->Shutdown_model->current_day ? $this->Shutdown_model->current_day : date('d'));
    }

}