<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Product_model extends MY_Model
{

    public $has_many      = array('product_photos');
    public $after_create  = array('create_dir');
    public $before_delete = array('delete_dir');
    public $belongs_to    = array('download_category');
    public $before_create = array('implode_colors');
    public $before_update = array('implode_colors');

    public function __construct()
    {
        parent::__construct();
        $this->load->model('Product_photo_model');
    }

    protected function implode_colors($product)
    {
        if (isset($product['product_colors']))
        {
            $product['product_colors'] = implode(',', $product['product_colors']);
        }
        return $product;
    }

    public function all_pagined($per_page = NULL)
    {
        return $this->order_by('created_at', 'DESC')->limit($per_page ? $per_page : $this->per_page, $this->input->get('page'))->get_all();
    }

    public function get_with_photos($id)
    {
        return $this->with('product_photos')->get($id);
    }

    public function create_dir($product)
    {
        mkdir($this->Product_photo_model->_upload_path . $product);
        return $product;
    }

    public function delete_dir($product)
    {
        $this->Product_photo_model->delete_photos($product);
        rmdir($this->Product_photo_model->_upload_path . $product);
        return $product;
    }

}