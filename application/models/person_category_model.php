<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Person_category_model extends MY_model
{

    public $has_many = array('people');

    public function get_all_with_people($id)
    {
        return $this->with('people')->get($id);
    }

}