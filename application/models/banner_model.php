<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Banner_model extends MY_Model
{

    public $_upload_path  = 'assets/uploads/banners/';
    public $_upload_types = 'gif|jpg|png';
    public $before_create = array('implode_pages');
    public $before_update = array('implode_pages');
    public $before_delete = array('delete_file');

    protected function implode_pages($banner)
    {
        if (isset($banner['pages']))
        {
            $banner['pages'] = implode(',', $banner['pages']);
        }
        return $banner;
    }

    protected function delete_file($banner)
    {
        @unlink($banner['url']);
        return $banner;
    }

    public function all_enabled()
    {
        $now    = date(FORMAT_DATE_TIME);
        $this->_database->where("(`start_at` IN ('0000-00-00 00:00:00', NULL)", NULL, FALSE);
        $this->_database->or_where("`start_at` is NULL ", NULL, FALSE);
        $this->_database->or_where("`start_at` <= '{$now}')", NULL, FALSE);
        $this->_database->where("(`end_at` IN ('0000-00-00 00:00:00', NULL)", NULL, FALSE);
        $this->_database->or_where("`end_at` is NULL ", NULL, FALSE);
        $this->_database->or_where("`end_at` >= '{$now}')", NULL, FALSE);
        $this->_database->where("(`pages` like '{$this->page_id},%'", NULL, FALSE);
        $this->_database->or_where("`pages` like '%,{$this->page_id},%'", NULL, FALSE);
        $this->_database->or_where("`pages` like '%,{$this->page_id}')", NULL, FALSE);
        $this->_database->order_by('sequence');
        $return = $this->get_many_by('enabled', '1');
        return $return;
    }

}