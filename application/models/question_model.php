<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Question_model extends MY_Model
{
    public $belongs_to    = array('question_category');
    public $_search_by    = array('title', 'answer');

    public function get_all_enabled()
    {
        return $this->order_by('important', 'DESC')->get_many_by('enabled', '1');
    }

}