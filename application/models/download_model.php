<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Download_model extends MY_model
{

    public $_upload_path  = 'assets/uploads/downloads/';
    public $_upload_types = 'pdf|doc|docx';
    public $belongs_to    = array('download_category');

    public function get_enabled($id)
    {
        return $this->get_by(array('id' => $id, 'enabled' => '1'));
    }

}