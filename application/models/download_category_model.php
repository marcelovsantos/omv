<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Download_category_model extends MY_model
{

    public $has_many = array('downloads');

    public function get_all_with_downloads()
    {
        return $this->with('downloads')->get_all();
    }

    public function get_many_with_downloads($ids)
    {
        $this->load->model('download_model');
        $download_categories = $this->get_many_by('id', explode(',', $ids));
        foreach ($download_categories as $download_category)
        {
            $downloads                    = new Download_model();
            $download_category->downloads = $downloads->order_by('created_at', 'DESC')->get_many_by(array(
                'download_category_id' => $download_category->id
                )
            );
        }
        return $download_categories;
    }

}