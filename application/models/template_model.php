<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Template_model extends MY_model
{

    public $has_many      = array('log_templates');
    public $_upload_path  = 'assets/uploads/templates/';
    public $before_update = array('create_log');

    public function get($id)
    {
        parent::with('log_templates');
        return parent::get($id);
    }

    public function create_log($template)
    {
        $this->load->model('Log_template_model');
        $backup = $this->get($template['id']);
        $this->Log_template_model->insert(array(
            'template_id' => $backup->id,
            'content'     => $backup->content
        ));
        return $template;
    }

}