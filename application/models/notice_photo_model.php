<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Notice_photo_model extends MY_Model
{
    public $_search_by = array();
    public $_upload_path  = 'assets/uploads/notice_photos/';
    public $_upload_types = 'gif|jpg|png';

    public function favorite($notice_photo)
    {
        $this->_database->query('UPDATE notice_photos SET  `sequence` = IF(id = ?, 0, 1) WHERE notice_id = ?', array($notice_photo->id, $notice_photo->notice_id));
    }

    public function delete_photos($notice_id)
    {
        $notice_photos = $this->get_many_by('notice_id', $notice_id);
        $ids    = array();

        foreach ($notice_photos as $notice_photo)
        {
            $ids[] = $notice_photo->id;
            @unlink($notice_photo->url);
        }

        if ($ids)
        {
            $this->delete_many($ids);
        }
    }

    public function delete_photo($notice_photo)
    {
        $url = $this->get($notice_photo);
        @unlink($url->url);
        return $notice_photo;
    }

}