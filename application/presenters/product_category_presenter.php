<?php

class Product_category_presenter extends Presenter
{

    public $v_map = array(
        'remove_product_category' => array('product_category_id'),
        'product_categories_html' => array(),
    );
    
    public function transform_remove_product_category($product_category_id){
        if($product_category_id)
        {
            return 'hide js-remove';
        }
    }
    
    public function transform_product_categories_html(){
        $category = $this->get_active();
        if(isset($category->product_categories) && $category->product_categories)
        {
            $html = '<ul class="list-unstyled">';
            foreach($category->product_categories as $category)
            {
                $html.= '<li>'.anchor("categorias/{$category->id}/produtos", $category->title).'</li>';
            }
            $html.= '</ul>';
            return $html;
        }
    }
}