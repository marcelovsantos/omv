<?php

class Warning_presenter extends Presenter
{

    public $v_map = array(
        'content_preview' => 'content'
    );

    public function transform_content_preview($content)
    {
        return substr(strip_tags($content), 0, 300);
    }

    public function transform_title($title)
    {
        return strtoupper($title);
    }

    public function transform_created_at($created_at)
    {
        return date(FORMAT_DATE_BR, strtotime($created_at));
    }

}