<?php

class Flag_presenter extends Presenter
{

    public function transform_date($date)
    {
        return month_to_br(date('F/Y', strtotime($date)));
    }
    
    public function transform_color($color)
    {
        $this->ci->load->language('cms');
        return t($color);
    }

}