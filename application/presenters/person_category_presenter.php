<?php

class Person_category_presenter extends Presenter
{

    public function transform_people($person = array())
    {
        return $this->create('person', $person);
    }

}