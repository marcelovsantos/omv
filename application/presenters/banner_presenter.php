<?php

class Banner_presenter extends Presenter
{

    protected $v_map = array(
        'active'   => array()
    );

    public function transform_active()
    {
        return $this->get_active() == array_shift(array_values($this->_result_set)) ? 'active' : '';
    }
}