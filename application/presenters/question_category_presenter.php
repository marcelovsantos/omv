<?php

class Question_category_presenter extends Presenter
{

    public function transform_questions($questions = array())
    {
        if ($questions)
        {
            return $this->create('question', $questions);
        }
        return new Presenter_single('<p>Não há arquivos cadastrados.</p>');
    }

}