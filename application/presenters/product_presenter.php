<?php

class Product_presenter extends Presenter
{

    private $divrow4 = 0;
    public $v_map = array(
        'hide_photos'     => array(),
        'hide_controls'   => array(),
        'content_preview' => array('content'),
    );

    public function __construct($result_set = NULL)
    {
        parent::__construct($result_set);

        if ($this->is_multi)
        {
            $this->v_map['first_image'] = array('product_photos');
            $this->v_map['image']       = array('product_photos');
            $this->v_map['divrow4']     = NULL;
        }
    }

    public function transform_divrow4()
    {
        $this->divrow4++;
        $div = ($this->divrow4 * 4) % 12 ? NULL : '</div><div class="row m-t-xl">';
        return $div;
    }

    public function transform_content_preview($content)
    {
        return substr(strip_tags($content), 0, 200) . '...';
    }

    public function transform_first_image($product_photos)
    {
        if ($product_photos && $this->get_active() == array_shift(array_values($this->_result_set)))
        {
            return $product_photos->first_image();
        }
    }

    public function transform_image($product_photos)
    {
        if ($product_photos)
        {
            return $product_photos->first_src();
        }
    }

    public function transform_hide_photos()
    {
        return (($active = $this->get_active()) && isset($active->product_photos) && $active->product_photos) ? '' : 'hide';
    }

    public function transform_hide_controls()
    {
        return (($active = $this->get_active()) && isset($active->product_photos) && count($active->product_photos)) ? '' : 'hide';
    }

    public function transform_created_at($created_at)
    {
        return date(FORMAT_DATE_TIME_BR, strtotime($created_at));
    }

    public function transform_product_photos($photos)
    {
        if ($photos)
        {
            return $this->create('product_photo', $photos);
        }
    }

    public function transform_product_colors($product_colors)
    {
        $html = '';
        foreach(explode(',',$product_colors) as $color)
        {
            if($color)
            {
                list($color, $title) = explode(':', $color);
                $html.= '<span class="product-color" style="background-color:'.$color.'" title="'.$title.'"></span>';
            }
        }
        return $html;
    }

}