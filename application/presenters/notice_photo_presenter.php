<?php

class Notice_photo_presenter extends Presenter
{

    public $divrow2 = 0;
    protected $v_map = array(
        'active'   => array(),
        'divrow2' => array(),
    );

    public function __construct($result_set = NULL)
    {
        parent::__construct($result_set);
        $this->first = array_shift(array_values($this->_result_set));
    }

    public function transform_active()
    {
        return $this->get_active() == $this->first ? 'active' : '';
    }

    public function transform_divrow2()
    {
        $this->divrow2++;
        $div = ($this->divrow2 * 2) % 12 ? NULL : '</div><div class="row">';
        return $div;
    }

    public function first_src()
    {
        if ($this->_result_set)
        {
            if (($photo = array_shift(array_values($this->_result_set))) && $photo->url)
            {
                return base_url($photo->url);
            }
        }
    }
    
    public function transform_url($url)
    {
        return base_url($url);
    }

    public function first_image()
    {
        if ($this->_result_set)
        {
            if (($photo = array_shift(array_values($this->_result_set))) && $photo->url)
            {
                return anchor("noticias/{$photo->notice_id}", img(array('src' => base_url($photo->url), 'class' => 'img-responsive fade in inline-block')));
            }
        }
    }

}