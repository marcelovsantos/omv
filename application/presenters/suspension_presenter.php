<?php

class Suspension_presenter extends Presenter
{

    public $v_map = array(
        'day'   => array('date'),
        'month' => array('date'),
        'year' => array('date'),
    );

    public function transform_date($date = NULL)
    {
        return date(FORMAT_DATE_BR, strtotime($date));
    }

    public function transform_day($date)
    {
        return date('d', strtotime($date));
    }

    public function transform_month($date)
    {
        return month_to_br(date('M', strtotime($date)));
    }

    public function transform_year($date)
    {
        return month_to_br(date('Y', strtotime($date)));
    }
    
}