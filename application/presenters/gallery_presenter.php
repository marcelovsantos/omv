<?php

class Gallery_presenter extends Presenter
{

    public $divrow = 0;
    public $v_map  = array(
        'day'             => array('created_at'),
        'month'           => array('created_at'),
        'year'            => array('created_at'),
        'content_preview' => array('content'),
        'slug'            => array('id'),
    );

    public function __construct($result_set = NULL)
    {
        parent::__construct($result_set);

        if ($this->is_multi)
        {
            $this->v_map['first_src']   = array('gallery_photos');
            $this->v_map['first_image'] = array('gallery_photos');
            $this->v_map['image']       = array('gallery_photos');
            $this->v_map['divrow']      = NULL;
        }
    }

    public function transform_divrow()
    {
        $this->divrow++;
        $div = ($this->divrow * 4) % 12 ? NULL : '</div><div class="row">';
        return $div;
    }

    public function transform_slug($id)
    {
        return $id;
    }

    public function transform_first_src($gallery_photos)
    {
//        if ($gallery_photos && $this->get_active() == array_shift(array_values($this->_result_set)))
        {
            return $gallery_photos->first_src();
        }
    }

    public function transform_image($gallery_photos)
    {
        if ($gallery_photos)
        {
            return $gallery_photos->first_src();
        }
    }

    public function transform_created_at($created_at)
    {
        return date(FORMAT_DATE_BR, strtotime($created_at));
    }

    public function transform_content_preview($content)
    {
        return substr(strip_tags($content), 0, 150) . '...';
    }

    public function transform_gallery_photos($photos)
    {
        if ($photos)
        {
            return $this->create('gallery_photo', $photos);
        }
    }

}