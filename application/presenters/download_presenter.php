<?php

class Download_presenter extends Presenter
{

    private $divrow4 = 0;

    public function __construct($result_set = NULL)
    {
        parent::__construct($result_set);

        if ($this->is_multi)
        {
            $this->v_map['divrow4'] = NULL;
        }
    }

    public function transform_id($id)
    {
        return $this->get_active()->enabled ? $id : '';
    }

    public function transform_title($title)
    {
        return $this->get_active()->enabled ? $title : "{$title} (Indisponível)";
    }

    public function transform_created_at($created_at)
    {
        return date(FORMAT_DATE_BR, strtotime($created_at));
    }

    public function transform_divrow4()
    {
        $this->divrow4++;
        $div = ($this->divrow4 * 4) % 12 ? NULL : '</div><div class="row">';
        return $div;
    }

}