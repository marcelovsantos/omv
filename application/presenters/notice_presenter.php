<?php

class Notice_presenter extends Presenter
{

    public $divrow6 = 0;
    public $v_map = array(
        'day'             => array('created_at'),
        'month'           => array('created_at'),
        'year'            => array('created_at'),
        'content_preview' => array('content'),
    );

    public function __construct($result_set = NULL)
    {
        parent::__construct($result_set);

        if ($this->is_multi)
        {
            $this->v_map['first_src'] = array('notice_photos');
            $this->v_map['first_image'] = array('notice_photos');
            $this->v_map['image']       = array('notice_photos');
            $this->v_map['divrow6']     = NULL;
        }
    }

    public function transform_divrow6()
    {
        $this->divrow6++;
        $div = ($this->divrow6 * 6) % 12 ? NULL : '</div><div class="row m-t-lg">';
        return $div;
    }

    public function transform_first_src($notice_photos)
    {
        if ($notice_photos && ($src = $notice_photos->first_src())) {
            return $src;
        }
        return base_url('assets/uploads/templates/1/images/notice-photo-default.png');
    }

    public function transform_image($notice_photos)
    {
        if ($notice_photos)
        {
            return $notice_photos->first_src();
        }
    }

    public function transform_created_at($created_at)
    {
        return date(FORMAT_DATE_BR, strtotime($created_at));
    }


    public function transform_content_preview($content)
    {
        return substr(strip_tags($content), 0, 150) . '...';
    }

    public function transform_notice_photos($photos)
    {
        if ($photos)
        {
            return $this->create('notice_photo', $photos);
        }
    }

}