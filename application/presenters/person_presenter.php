<?php

class Person_presenter extends Presenter
{

    public $v_map = array(
        'divrow4' => array()
    );

    public function transform_url($url)
    {
        if (!$url)
        {
            $url = 'assets/uploads/templates/1/images/person-default.png';
        }
        return $url;
    }

    public function transform_observation($observation)
    {
        return t($observation);
    }
    
        public function transform_divrow4()
    {
        $this->divrow4++;
        $div = ($this->divrow4 * 4) % 12 ? NULL : '</div><div class="row">';
        return $div;
    }

}