<?php

class Product_photo_presenter extends Presenter
{

    public $divitem4 = 0;
    protected $v_map = array(
        'active'   => array(),
        'divitem4' => array(),
    );

    public function __construct($result_set = NULL)
    {
        parent::__construct($result_set);
        $this->first = array_shift(array_values($this->_result_set));
    }

    public function transform_active()
    {
        return $this->get_active() == $this->first ? 'active' : '';
    }

    public function transform_divitem4()
    {
        $this->divitem4++;
        $div = ($this->divitem4 * 4) % 12 ? NULL : '</div><div class="item">';
        return $div;
    }

    public function first_src()
    {
        if ($this->_result_set)
        {
            if (($photo = array_shift(array_values($this->_result_set))) && $photo->url)
            {
                return base_url($photo->url);
            }
        }
    }

    public function first_image()
    {
        if ($this->_result_set)
        {
            if (($photo = array_shift(array_values($this->_result_set))) && $photo->url)
            {
                return img(array('src' => base_url($photo->url), 'class' => 'img-responsive'));
            }
        }
    }

}