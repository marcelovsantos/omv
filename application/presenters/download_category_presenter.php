<?php

class Download_category_presenter extends Presenter
{

    public function transform_downloads($downloads = array())
    {
        if ($downloads)
        {
            return $this->create('download', $downloads);
        }
        return new Presenter_single('<p>Não há arquivos cadastrados.</p>');
    }

}