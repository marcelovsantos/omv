<?php
$lang['cal_su']        = "D";
$lang['cal_mo']        = "S";
$lang['cal_tu']        = "T";
$lang['cal_we']        = "Q";
$lang['cal_th']        = "Q";
$lang['cal_fr']        = "S";
$lang['cal_sa']        = "S";
$lang['cal_sun']       = "D";
$lang['cal_mon']       = "Mon";
$lang['cal_tue']       = "Tue";
$lang['cal_wed']       = "Wed";
$lang['cal_thu']       = "Thu";
$lang['cal_fri']       = "Fri";
$lang['cal_sat']       = "Sat";
$lang['cal_sunday']    = "Sunday";
$lang['cal_monday']    = "Monday";
$lang['cal_tuesday']   = "Tuesday";
$lang['cal_wednesday'] = "Wednesday";
$lang['cal_thursday']  = "Thursday";
$lang['cal_friday']    = "Friday";
$lang['cal_saturday']  = "Saturday";
$lang['cal_jan']       = "Jan";
$lang['cal_feb']       = "Feb";
$lang['cal_mar']       = "Mar";
$lang['cal_apr']       = "Apr";
$lang['cal_may']       = "May";
$lang['cal_jun']       = "Jun";
$lang['cal_jul']       = "Jul";
$lang['cal_aug']       = "Aug";
$lang['cal_sep']       = "Sep";
$lang['cal_oct']       = "Oct";
$lang['cal_nov']       = "Nov";
$lang['cal_dec']       = "Dec";
$lang['cal_january']   = "Janeiro";
$lang['cal_february']  = "Fevereiro";
$lang['cal_march']     = "Março";
$lang['cal_april']     = "Abril";
$lang['cal_mayl']      = "Maio";
$lang['cal_june']      = "Junho";
$lang['cal_july']      = "Julho";
$lang['cal_august']    = "Agosto";
$lang['cal_september'] = "Setembro";
$lang['cal_october']   = "Outubro";
$lang['cal_november']  = "Novembro";
$lang['cal_december']  = "Dezembro";


/* End of file calendar_lang.php */
/* Location: ./system/language/english/calendar_lang.php */