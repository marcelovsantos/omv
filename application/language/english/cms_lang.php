<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

$lang['access']       = 'Acessar';
$lang['enabled']      = 'Ativo';
$lang['disabled']     = 'Inativo';
$lang['back']         = 'Ativo';
$lang['back']         = 'Voltar';
$lang['save']         = 'Salvar';
$lang['cancel']       = 'Cancelar';
$lang['edit']         = 'Editar';
$lang['send']         = 'Enviar';
$lang['delete']       = 'Excluir';
$lang['new']          = 'Novo';
$lang['options']      = 'Opções';
$lang['enter']        = 'Entrar';
$lang['signin']       = 'Login';
$lang['help']         = 'Ajuda';
$lang['informations'] = 'Informações';
$lang['exit']         = 'Sair';
$lang['date']         = 'Data';
$lang['time']         = 'Hora';
$lang['sequence']     = 'Ordem';

$lang['error_signin'] = 'Não foi possível se conectar.';
$lang['user']         = 'Usuário';
$lang['password']     = 'Senha';

$lang['created_at']      = 'Criado em';
$lang['updated_at']      = 'Atualizado em';
$lang['place']           = 'Local';
$lang['observation']     = 'Observação';
$lang['phone_secondary'] = 'Telefone Secundário';
$lang['phone']           = 'Telefone';
$lang['name']            = 'Nome';
$lang['important']       = 'Destaque';
$lang['start_at']        = 'Inicia em';
$lang['end_at']          = 'Termina em';
$lang['id']              = 'Código';
$lang['url']             = 'URL';
$lang['title']           = 'Título';
$lang['content']         = 'Conteúdo';
$lang['visible']         = 'Visível';
$lang['link']            = 'Link';
$lang['description']     = 'Descrição';
$lang['keywords']        = 'Palavras-chave';
$lang['image']           = 'Imagem';
$lang['file']            = 'File';
$lang['scripts']         = 'Scripts';
$lang['styles']          = 'Estilos';
$lang['source']          = 'Fonte';
$lang['author']          = 'Autor';
$lang['new_password']    = 'Nova Senha';
$lang['repeat_password'] = 'Repita a Senha';
$lang['login']           = 'Login';
$lang['email']           = 'E-mail';
$lang['effective']       = 'Efetivo';
$lang['surrogate']       = 'Suplente';
$lang['answer']          = 'Resposta';
$lang['role']            = 'Cargo';
$lang['color']           = 'Cor';
$lang['colors']          = 'Cores';

$lang['revert_changes']     = 'Reverter Alterações';
$lang['toogle_html_editor'] = 'Alternar Texto/HTML';

$lang['page']      = 'Página';
$lang['pages']     = 'Páginas';
$lang['edit_page'] = 'Editar Página';
$lang['new_page']  = 'Nova Página';

$lang['user']      = 'Usuário';
$lang['users']     = 'Usuários';
$lang['edit_user'] = 'Editar Usuário';
$lang['new_user']  = 'Nova Usuário';

$lang['notice']      = 'Notícia';
$lang['notices']     = 'Notícias';
$lang['edit_notice'] = 'Editar Notícia';
$lang['new_notice']  = 'Nova Notícia';

$lang['gallery']      = 'Linha';
$lang['galleries']    = 'Linhas';
$lang['edit_gallery'] = 'Editar Linha';
$lang['new_gallery']  = 'Nova Linha';

$lang['photo']      = 'Foto';
$lang['photos']     = 'Fotos';
$lang['edit_photo'] = 'Editar Foto';
$lang['new_photo']  = 'Nova Foto';
$lang['new_photos'] = 'Novas Fotos';

$lang['template']       = 'Template';
$lang['templates']      = 'Templates';
$lang['edit_template']  = 'Editar Template';
$lang['new_template']   = 'Novo Template';
$lang['files']          = 'Arquivos';
$lang['files_download'] = 'Baixar Arquivos';

$lang['banner']      = 'Baner';
$lang['banners']     = 'Baners';
$lang['edit_banner'] = 'Editar Baner';
$lang['new_banner']  = 'Novo Baner';

$lang['block']      = 'Bloco';
$lang['blocks']     = 'Blocos';
$lang['edit_block'] = 'Editar Bloco';
$lang['new_block']  = 'Novo Bloco';

$lang['download']      = 'Download';
$lang['downloads']     = 'Downloads';
$lang['edit_download'] = 'Editar Download';
$lang['new_download']  = 'Novo Download';

$lang['download_category']      = 'Categoria de Download';
$lang['download_categories']    = 'Categorias de Download';
$lang['edit_download_category'] = 'Editar Categoria de Download';
$lang['new_download_category']  = 'Nova Categoria de Download';

$lang['shutdown']      = 'Desligamento';
$lang['shutdowns']     = 'Desligamentos';
$lang['edit_shutdown'] = 'Editar Desligamento';
$lang['new_shutdown']  = 'Novo Desligamento';

$lang['suspension']      = 'Suspensão';
$lang['suspensions']     = 'Suspensões';
$lang['edit_suspension'] = 'Editar Suspensão';
$lang['new_suspension']  = 'Nova Suspensão';

$lang['question']      = 'Pergunta';
$lang['questions']     = 'Perguntas';
$lang['edit_question'] = 'Editar Pergunta';
$lang['new_question']  = 'Nova Pergunta';

$lang['question_category']      = 'Categoria de Pergunta';
$lang['question_categories']    = 'Categorias de Pergunta';
$lang['edit_question_category'] = 'Editar Categoria de Pergunta';
$lang['new_question_category']  = 'Nova Categoria de Pergunta';

$lang['warning']      = 'Aviso';
$lang['warnings']     = 'Avisos';
$lang['edit_warning'] = 'Editar Aviso';
$lang['new_warning']  = 'Novo Aviso';

$lang['person']      = 'Pessoa';
$lang['people']      = 'Pessoas';
$lang['edit_person'] = 'Editar Pessoa';
$lang['new_person']  = 'Nova Pessoa';

$lang['person_category']      = 'Categoria de Pessoa';
$lang['person_categories']    = 'Categorias de Pessoa';
$lang['edit_person_category'] = 'Editar Categoria de Pessoa';
$lang['new_person_category']  = 'Nova Categoria de Pessoa';

$lang['flag']      = 'Bandeira';
$lang['flags']     = 'Bandeiras';
$lang['edit_flag'] = 'Editar Bandeira';
$lang['new_flag']  = 'Nova Bandeira';
$lang['green']     = 'Verde';
$lang['yellow']    = 'Amarela';
$lang['red']       = 'Vermelha';

$lang['product']      = 'Produto';
$lang['products']     = 'Produtos';
$lang['edit_product'] = 'Editar Produto';
$lang['new_product']  = 'Novo Produto';

$lang['product_category']      = 'Categoria de Produto';
$lang['product_categories']    = 'Categorias de Produto';
$lang['edit_product_category'] = 'Editar Categoria de Produto';
$lang['new_product_category']  = 'Nova Categoria de Produto';

$lang['product_color']      = 'Cor de Produto';
$lang['product_colors']     = 'Cores de Produto';
$lang['edit_product_color'] = 'Editar Cor de Produto';
$lang['new_product_color']  = 'Nova Cor de Produto';

$lang['subscription']      = 'Inscrição';
$lang['subscriptions']     = 'Inscrições';
$lang['edit_subscription'] = 'Editar Inscrição';
$lang['new_subscription']  = 'Nova Inscrição';

$lang['more'] = 'Mais';

$lang['success_create'] = 'Criado com sucesso!';
$lang['success_update'] = 'Atualizado com sucesso!';
$lang['error_create']   = 'Não foi possível cadastrar!';
$lang['error_update']   = 'Não foi possível atualizar!';

$lang['enabled-1']   = "<i class=\"fa fa-check-circle-o\"></i> {$lang['enabled']}";
$lang['enabled-0']   = 'Inativo';
$lang['enabled-']    = $lang['enabled-0'];
$lang['important-1'] = "<i class=\"fa fa-arrow-circle-up\"></i> {$lang['important']}";
$lang['important-0'] = 'Normal';
$lang['important-']  = $lang['important-0'];
